package main

import (
	"context"
	"log"
	"os"
	"syscall"
	"time"

	"go.uber.org/zap/zapcore"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/growth-api/internal/service"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/grpcclient"
	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/core"
	"gitlab.com/picnic-app/backend/libs/golang/core/mw"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/graceful"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/monitoring"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	profileV2 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	err := config.Load(ctx)
	if err != nil {
		log.Fatal(err)
	}

	initLog(ctx, config.LogLevel())

	app, err := initApp(ctx)
	if err != nil {
		log.Fatal(err)
	}
	cfg := app.Config()

	err = tracing.SetupExporter(ctx)
	if err != nil {
		logger.Errorf(ctx, "failed to set up tracing exporter: %v", err)
	}

	monitoring.RegisterPrometheusSuffix()

	debugSrv, err := app.RunDebug(ctx)
	if err != nil {
		logger.Fatalf(ctx, "failed to start debug server: %v", err)
	}

	ctrl, cleanup := initController(ctx, cfg)

	gracefulShutdown := graceful.New(
		&graceful.ShutdownManagerOptions{Timeout: 60 * time.Second},
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "servers",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(func() error {
				app.Close()
				return nil
			}),
			graceful.HTTPServer(debugSrv),
		),
		graceful.Context(cancel),
		graceful.Parallel(
			&graceful.ParallelShutdownOptions{
				Name:    "clients",
				Timeout: 30 * time.Second,
			},
			graceful.ShutdownErrorFunc(cleanup),
			graceful.Tracer(),
		),
		graceful.Logger(nil),
	)
	gracefulShutdown.RegisterSignals(os.Interrupt, syscall.SIGTERM)
	defer func() {
		_ = gracefulShutdown.Shutdown(context.Background())
	}()

	err = app.Run(ctx, ctrl)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	logger.Info(ctx, "gRPC server closed gracefully")
}

func initLog(ctx context.Context, logLevelCfg string) {
	logLevel, err := zapcore.ParseLevel(logLevelCfg)
	if err != nil {
		log.Fatal(ctx, err)
	}

	logger.SetLevel(logLevel)
}

func initApp(ctx context.Context) (core.Application, error) {
	app, err := core.InitAppWithOptions(ctx, mw.LogOptions{
		RedactRequestFunc:  func(a any) any { return a },
		RedactResponseFunc: func(a any) any { return a },
	})
	if err != nil {
		return nil, err
	}

	app = app.WithMW(mw.NewServerContextInterceptor(config.String("env.auth.secret")))

	return app, nil
}

func initController(ctx context.Context, cfg config.Config) (controller.Controller, func() error) {
	db, err := spanner.NewSpannerClient(ctx, cfg.Spanner.DSN, cfg.Spanner.ADCPath)
	if err != nil {
		logger.Fatalf(ctx, "failed to create spanner client: %v", err)
	}

	repo := spanner.NewRepoWithClient(db)

	eventbusClient, err := eventbus.DefaultClient(ctx)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	// profile-api client
	profileConn := grpcclient.New(ctx,
		config.String("env.svc.profile.host"),
		config.String("env.svc.profile.port"),
		config.Bool("env.svc.profile.secure"),
		cfg.ServiceName,
		config.String("env.profile.secret"),
	)
	profileClient := profileV2.NewProfileServiceClient(profileConn)

	svc := service.New(
		repo,
		profileClient,
	)

	err = svc.InitRepo(ctx)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	switch config.String("env.service.mode") {
	case "":
		logger.Info(ctx, "Running in server mode")
	case "worker":
		logger.Info(ctx, "Running in worker mode")
		err = svc.ListenEvents(ctx, eventbusClient)
		if err != nil {
			logger.Fatal(ctx, err)
		}
	}

	return controller.New(svc), func() error {
		eventbusClient.Close()
		db.Close()
		_ = profileConn.Close()
		return nil
	}
}
