module "pubsub" {
  source  = "gitlab.com/picnic-app/pubsub/google"
  version = "~> 1.0"

  env_id       = var.env_id
  service_name = var.service_name
  sa_email     = module.workload-identity.service_account.email

  subscriptions = [
    "app",
    "chat",
    "circle",
    "content",
    "comment",
    "profile",
  ]

  publishes = ["notification_ingest"]
}
