module "spanner-database" {
  source  = "gitlab.com/picnic-app/spanner-database/google"
  version = "~> 1.0"

  env_id       = var.env_id
  service_name = var.service_name
  sa_email     = module.workload-identity.service_account.email

  deletion_protection      = local.is_not_dev
  version_retention_period = local.is_not_dev ? "1d" : "1h"
}
