package service

import (
	"context"
	"time"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetCurrentUserStreak(ctx context.Context, streakID string) (*model.UserStreak, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if streakID == "" {
		return nil, ErrRequiredParam("streak id")
	}

	us, err := s.repo.SingleRead().GetLastUserStreak(ctx, streakID, userID)
	if err != nil || us == nil {
		return nil, err
	}

	// If the last streak is not overdue yet.
	if time.Now().Before(us.LastBlockEndsAt) {
		return serialize.Pointer(us, serialize.UserStreak), nil
	}

	return nil, nil
}
