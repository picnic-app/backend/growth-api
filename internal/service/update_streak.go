package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) UpdateStreak(ctx context.Context, streakID string, in model.UpdateStreakInput) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	if in == (model.UpdateStreakInput{}) {
		return nil
	}

	return s.repo.SingleWrite().UpdateStreak(
		ctx,
		repo.UpdateStreakInput{
			StreakID:        streakID,
			UserID:          userID,
			Name:            in.Name,
			Description:     in.Description,
			EventsPerPeriod: in.EventsPerPeriod,
			PeriodDuration:  in.PeriodDuration,
		},
	)
}
