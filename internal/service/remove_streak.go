package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) RemoveStreak(ctx context.Context, streakID string) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	_, err = s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		streaks, err := tx.GetStreaks(ctx, streakID)
		if err != nil {
			return err
		}

		if len(streaks) == 0 {
			return ErrNotFound
		}

		if streaks[0].DeletedAt != nil {
			return nil
		}

		return tx.RemoveStreak(ctx, streakID, userID)
	})

	return err
}
