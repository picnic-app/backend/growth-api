package service

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
)

func (s *Service) ListenEvents(ctx context.Context, client eventbus.Client) error {
	for _, topic := range []string{
		eventbus.TopicID().App,
		eventbus.TopicID().Chat,
		eventbus.TopicID().Circle,
		eventbus.TopicID().Comment,
		eventbus.TopicID().Content,
	} {
		err := client.Topic(topic).Sub(ctx, s.RecordEventInStreak)
		if err != nil {
			return errors.Wrapf(err, "subscribing to %q", topic)
		}
	}

	err := client.Topic(eventbus.TopicID().Profile).Sub(ctx, s.HandleProfileEvent)
	if err != nil {
		return fmt.Errorf("subscribing to %q: %w", eventbus.TopicID().Profile, err)
	}

	return nil
}

func HandlePayload[M any](ctx context.Context, evt *event.Event, f func(ctx context.Context, payload M) error) error {
	b, err := json.Marshal(evt.Payload)
	if err != nil {
		return errors.Wrap(err, "marshalling to json")
	}

	var payload M
	if err = json.Unmarshal(b, &payload); err != nil {
		return errors.Wrap(err, "unmarshalling from json")
	}

	return f(ctx, payload)
}

func (s *Service) HandleProfileEvent(ctx context.Context, evt *event.Event) error {
	ctx = auth.AddServiceNameToCtx(ctx, "profile-api")

	if err := s.RecordEventInStreak(ctx, evt); err != nil {
		return fmt.Errorf("failed recording event in streak while handling profile event: %w", err)
	}

	switch evt.EvtType {
	case event.TypeUserNew:
		return HandlePayload(ctx, evt, func(ctx context.Context, p event.NewUserPayload) error {
			if p.AffiliateCodeOwner == nil || *p.AffiliateCodeOwner == "" {
				return nil
			}

			if err := s.SaveAffiliateLinkSignup(ctx, *p.AffiliateCodeOwner, p.UserID); err != nil {
				return fmt.Errorf("saving affiliate link signup: %w", err)
			}

			return nil
		})
	default:
		return nil
	}
}
