package serialize

import (
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
)

func StreakCoins(sc repo.StreakCoins) model.StreakCoin {
	return model.StreakCoin{
		StreakID:         sc.StreakID,
		CoinsCount:       sc.CoinsCount,
		EveryStreakCount: sc.EveryStreakCount,
	}
}
