package serialize

import (
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
)

func UserStreak(us repo.UserStreak) model.UserStreak {
	return model.UserStreak{
		ID:                      us.ID,
		UserID:                  us.UserID,
		StreakID:                us.StreakID,
		StartedAt:               us.StartedAt,
		PeriodDuration:          us.PeriodDuration,
		LastBlockEndsAt:         us.LastBlockEndsAt,
		StreakLastsDuration:     us.StreakLastsDuration,
		CompletedBlocksCount:    us.CompletedBlocksCount,
		LastBlockNumberOfEvents: us.LastBlockNumberOfEvents,
		NumberOfEventsPerPeriod: us.NumberOfEventsPerPeriod,
	}
}
