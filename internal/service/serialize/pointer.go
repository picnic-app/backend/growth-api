package serialize

func Pointer[A, B any](v *A, f func(A) B) *B {
	if v == nil {
		return nil
	}

	out := f(*v)
	return &out
}
