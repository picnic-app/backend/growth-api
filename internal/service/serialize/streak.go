package serialize

import (
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
)

func Streak(s repo.Streak) model.Streak {
	return model.Streak{
		ID:              s.ID,
		Name:            s.Name,
		CreatedBy:       s.CreatedBy,
		Description:     s.Description,
		StreakType:      s.StreakType,
		EventsPerPeriod: s.EventsPerPeriod,
		PeriodDuration:  s.PeriodDuration,
		CreatedAt:       s.CreatedAt,
		DeletedAt:       s.DeletedAt,
		DeletedBy:       s.DeletedBy,
		UpdatedAt:       s.UpdatedAt,
		UpdatedBy:       s.UpdatedBy,
	}
}
