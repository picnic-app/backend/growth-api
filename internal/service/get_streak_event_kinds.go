package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetStreakEventKinds(ctx context.Context, streakID string) ([]model.EventKind, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if streakID == "" {
		return nil, ErrRequiredParam("streak id")
	}

	return s.repo.SingleRead().GetStreakEventKinds(ctx, streakID)
}
