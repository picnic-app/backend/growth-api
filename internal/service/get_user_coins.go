package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetUserCoins(ctx context.Context, userID string) (int64, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return 0, err
	}

	if userID == "" {
		return 0, ErrRequiredParam("user id")
	}

	return s.repo.SingleRead().GetUserCoins(ctx, userID)
}
