package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/slice"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetStreakCoins(ctx context.Context, streakID string) ([]model.StreakCoin, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if streakID == "" {
		return nil, ErrRequiredParam("streak id")
	}

	sc, err := s.repo.SingleRead().GetStreakCoins(ctx, streakID)
	return slice.Convert(serialize.StreakCoins, sc...), err
}
