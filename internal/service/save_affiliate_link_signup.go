package service

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
)

func (s *Service) SaveAffiliateLinkSignup(
	ctx context.Context,
	affiliateLinkOwnerUserID string,
	signedUpUserID string,
) error {
	if affiliateLinkOwnerUserID == "" {
		return ErrRequiredParam("affiliate link owner user id")
	}

	if signedUpUserID == "" {
		return ErrRequiredParam("signed up user id")
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exists, err := tx.ExistsAffiliateLinkSignup(ctx, affiliateLinkOwnerUserID, signedUpUserID)
		if err != nil {
			return errors.WithStack(err)
		}

		if exists {
			return nil
		}

		err = tx.InsertAffiliateLinkSignup(ctx, affiliateLinkOwnerUserID, signedUpUserID)
		if err != nil {
			return errors.WithStack(err)
		}

		stats, err := tx.GetAffiliateLinkStatistics(ctx, affiliateLinkOwnerUserID)
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				stats = repo.AffiliateLinkStatistics{
					OwnerID: affiliateLinkOwnerUserID,
				}
			} else {
				return errors.WithStack(err)
			}
		}

		stats.Signups++

		err = tx.UpsertAffiliateLinkStatistics(ctx, stats)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
