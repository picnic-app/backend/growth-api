package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetEarnedStreakCoins(ctx context.Context, streakID string) (int64, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return 0, err
	}

	if streakID == "" {
		return 0, ErrRequiredParam("streak id")
	}

	return s.repo.SingleRead().GetUserStreakCoins(ctx, streakID, userID)
}
