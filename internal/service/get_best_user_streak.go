package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetBestUserStreak(ctx context.Context, streakID string) (*model.UserStreak, error) {
	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if streakID == "" {
		return nil, ErrRequiredParam("streak id")
	}

	us, err := s.repo.SingleRead().GetBestStreakOfUser(ctx, streakID, userID)
	if err != nil || us == nil {
		return nil, err
	}

	return serialize.Pointer(us, serialize.UserStreak), nil
}
