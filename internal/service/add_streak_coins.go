package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) AddStreakCoins(ctx context.Context, streakID string, coinsCount, everyStreakCount int64) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	if _, err := auth.GetUserID(ctx); err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	if coinsCount < 0 {
		return ErrUnsupported("coins count", coinsCount)
	}

	if everyStreakCount <= 0 {
		return ErrUnsupported("every streak count", everyStreakCount)
	}

	return s.repo.SingleWrite().UpsertStreakCoins(ctx, streakID, coinsCount, everyStreakCount)
}
