package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	profileV2 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2"
)

func New(
	repo repo.Repo,
	profileClient profileV2.ProfileServiceClient,
) *Service {
	return &Service{
		repo:          repo,
		profileClient: profileClient,
	}
}

type Service struct {
	repo          repo.Repo
	profileClient profileV2.ProfileServiceClient
}

func (s *Service) InitRepo(ctx context.Context) error {
	return s.repo.SingleWrite().UpsertStreakType(ctx, model.StreakTypeUser, "user related streaks")
}
