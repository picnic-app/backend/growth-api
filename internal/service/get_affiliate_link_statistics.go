package service

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetAffiliateLinkStatistics(
	ctx context.Context,
	userID string,
) (*model.AffiliateLinkStatistics, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	if userID == "" {
		return nil, ErrRequiredParam("user id")
	}

	stats, err := s.repo.SingleRead().GetAffiliateLinkStatistics(ctx, userID)
	if err != nil && !errors.Is(err, repo.ErrNotFound) {
		return nil, err
	}

	return &model.AffiliateLinkStatistics{
		Clicks:             stats.Clicks,
		Signups:            stats.Signups,
		MobileAppDownloads: stats.MobileAppDownloads,
	}, nil
}
