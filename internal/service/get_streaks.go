package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/slice"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetStreaks(ctx context.Context, ids ...string) ([]model.Streak, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return nil, err
	}

	streaks, err := s.repo.SingleRead().GetNonDeletedStreaks(ctx, ids...)
	return slice.Convert(serialize.Streak, streaks...), err
}
