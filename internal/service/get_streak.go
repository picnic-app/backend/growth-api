package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetStreak(ctx context.Context, id string) (model.Streak, error) {
	_, err := auth.GetUserID(ctx)
	if err != nil {
		return model.Streak{}, err
	}

	if id == "" {
		return model.Streak{}, ErrRequiredParam("streak id")
	}

	streaks, err := s.repo.SingleRead().GetStreaks(ctx, id)
	if err != nil {
		return model.Streak{}, err
	}

	if len(streaks) == 0 {
		return model.Streak{}, ErrNotFound
	}

	streak := serialize.Streak(streaks[0])
	if streak.DeletedAt != nil {
		return streak, ErrDeleted
	}

	return streak, nil
}
