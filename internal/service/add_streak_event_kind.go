package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/util/is"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) AddStreakEventKind(ctx context.Context, streakID, eventKind string) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	_, err := auth.GetUserID(ctx)
	if err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	if eventKind == "" {
		return ErrRequiredParam("event kind")
	}

	streak, err := s.GetStreak(ctx, streakID)
	if err != nil {
		return err
	}

	allowed, err := s.repo.SingleRead().GetEventKindsByStreakType(ctx, streak.StreakType)
	if err != nil {
		return err
	}

	if !is.AnyOf(eventKind, allowed...) {
		return ErrUnsupported("event kind", eventKind)
	}

	return s.repo.SingleWrite().UpsertStreakEventKinds(ctx, streakID, eventKind)
}
