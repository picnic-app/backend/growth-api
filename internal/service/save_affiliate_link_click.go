package service

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	profileV2 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2"
)

func (s *Service) SaveAffiliateLinkClick(
	ctx context.Context,
	affiliateCode string,
	deviceFingerprint string,
) error {
	if affiliateCode == "" {
		return ErrRequiredParam("affiliate code")
	}

	if deviceFingerprint == "" {
		return ErrRequiredParam("device fingerprint")
	}

	rsp, err := s.profileClient.GetUserIDByAffiliateCodeWithoutAuth(
		ctx,
		&profileV2.GetUserIDByAffiliateCodeWithoutAuthRequest{
			AffiliateCode: affiliateCode,
		},
	)
	if err != nil {
		return errors.WithStack(err)
	}

	ownerID := rsp.GetUserId()

	_, err = s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		exists, err := tx.ExistsAffiliateLinkClick(ctx, ownerID, deviceFingerprint)
		if err != nil {
			return errors.WithStack(err)
		}

		if exists {
			return nil
		}

		err = tx.InsertAffiliateLinkClick(ctx, ownerID, deviceFingerprint)
		if err != nil {
			return errors.WithStack(err)
		}

		stats, err := tx.GetAffiliateLinkStatistics(ctx, ownerID)
		if err != nil {
			if errors.Is(err, repo.ErrNotFound) {
				stats = repo.AffiliateLinkStatistics{
					OwnerID: ownerID,
				}
			} else {
				return errors.WithStack(err)
			}
		}

		stats.Clicks++

		err = tx.UpsertAffiliateLinkStatistics(ctx, stats)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
