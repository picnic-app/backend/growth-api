package service

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) AddUserCoins(ctx context.Context, userID string, coins int64) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	if _, err := auth.GetUserID(ctx); err != nil {
		return err
	}

	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		current, err := tx.GetUserCoins(ctx, userID)
		if err != nil {
			return err
		}

		if current+coins < 0 {
			return ErrCoinsCannotBeNegative
		}

		return tx.InsertUserCoinTransaction(ctx, uuid.NewString(), userID, coins)
	})
	return err
}
