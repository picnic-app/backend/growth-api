package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) DeleteStreakEventKind(ctx context.Context, streakID string, eventKind model.EventKind) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	if _, err := auth.GetUserID(ctx); err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	if eventKind == "" {
		return ErrRequiredParam("event kind")
	}

	return s.repo.SingleWrite().DeleteStreakEventKind(ctx, streakID, eventKind)
}
