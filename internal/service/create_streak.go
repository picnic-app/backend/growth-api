package service

import (
	"context"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/is"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) CreateStreak(ctx context.Context, in model.CreateStreakInput) (model.Streak, error) {
	if !auth.IsAdmin(ctx) {
		return model.Streak{}, ErrNotAllowed
	}

	userID, err := auth.GetUserID(ctx)
	if err != nil {
		return model.Streak{}, err
	}

	err = s.ValidateCreateStreakInput(ctx, in)
	if err != nil {
		return model.Streak{}, err
	}

	id := uuid.NewString()
	createdAt, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		err = tx.InsertStreak(
			ctx,
			repo.InsertStreakInput{
				ID:              id,
				Name:            in.Name,
				CreatedBy:       userID,
				StreakType:      in.StreakType,
				Description:     in.Description,
				PeriodDuration:  in.PeriodDuration,
				EventsPerPeriod: in.EventsPerPeriod,
			},
		)
		if err != nil {
			return errors.WithStack(err)
		}

		return tx.UpsertStreakEventKinds(ctx, id, in.EventKinds...)
	})
	if err != nil {
		return model.Streak{}, err
	}

	out := model.Streak{
		ID:              id,
		Name:            in.Name,
		CreatedBy:       userID,
		Description:     in.Description,
		StreakType:      in.StreakType,
		EventsPerPeriod: in.EventsPerPeriod,
		PeriodDuration:  in.PeriodDuration,
		CreatedAt:       createdAt,
	}
	return out, nil
}

func (s *Service) ValidateCreateStreakInput(ctx context.Context, in model.CreateStreakInput) error {
	if in.Name == "" {
		return ErrRequiredParam("name")
	}
	if !is.AnyOf(in.StreakType, model.StreakTypeUser) {
		return ErrUnsupported("streak type", in.StreakType)
	}
	if in.EventsPerPeriod <= 0 {
		return ErrUnsupported("events per period", in.EventsPerPeriod)
	}
	if in.PeriodDuration <= 0 {
		return ErrUnsupported("period duration", in.PeriodDuration)
	}
	if len(in.EventKinds) == 0 {
		return ErrRequiredParam("event kinds")
	}
	allowedKinds, err := s.repo.SingleRead().GetEventKindsByStreakType(ctx, in.StreakType)
	if err != nil {
		return err
	}
	if len(allowedKinds) == 0 {
		return status.Errorf(
			codes.Internal,
			"streak type %q cannot be bound to any event",
			in.StreakType,
		)
	}
	if !isSubset(in.EventKinds, allowedKinds) {
		return status.Errorf(
			codes.InvalidArgument,
			"streak type %q cannot be activated for %q events",
			in.StreakType,
			in.EventKinds,
		)
	}

	return nil
}

func isSubset(subSet, set []string) bool {
	if len(subSet) == 1 {
		return is.AnyOf(subSet[0], set...)
	}

	s := make(map[string]struct{}, len(set))
	for _, k := range set {
		s[k] = struct{}{}
	}

	for _, k := range subSet {
		if _, ok := s[k]; !ok {
			return false
		}
	}

	return true
}
