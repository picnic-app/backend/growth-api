package service

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/is"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) GetEventKindsByStreakType(ctx context.Context, st model.StreakType) ([]string, error) {
	if !auth.IsAdmin(ctx) {
		return nil, ErrNotAllowed
	}

	if !is.AnyOf(st, model.StreakTypeUser) {
		return nil, ErrUnsupported("streak type", st)
	}

	return s.repo.SingleRead().GetEventKindsByStreakType(ctx, st)
}
