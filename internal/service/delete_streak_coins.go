package service

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
)

func (s *Service) DeleteStreakCoins(ctx context.Context, streakID string, everyStreakCount int64) error {
	if !auth.IsAdmin(ctx) {
		return ErrNotAllowed
	}

	if _, err := auth.GetUserID(ctx); err != nil {
		return err
	}

	if streakID == "" {
		return ErrRequiredParam("streak id")
	}

	return s.repo.SingleWrite().DeleteStreakCoins(ctx, streakID, everyStreakCount)
}
