package service

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func ErrRequiredParam(param string) error {
	return status.Errorf(codes.InvalidArgument, "parameter %q required", param)
}

func ErrUnsupported(param string, value any) error {
	return status.Errorf(codes.InvalidArgument, "value %v of parameter %q is not supported", value, param)
}

var (
	ErrNotAllowed            = status.Errorf(codes.PermissionDenied, "not allowed")
	ErrNotFound              = status.Errorf(codes.NotFound, "not found")
	ErrDeleted               = status.Errorf(codes.NotFound, "deleted")
	ErrCoinsCannotBeNegative = status.Errorf(codes.FailedPrecondition, "the number of coins cannot be negative")
)
