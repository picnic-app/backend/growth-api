package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/go-openapi/swag"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/service/serialize"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
)

func (s *Service) RecordEventInStreak(ctx context.Context, event *event.Event) error {
	buf, _ := bufPool.Get().(*bytes.Buffer)
	if buf == nil {
		buf = bytes.NewBuffer(nil)
	}
	defer bufPool.Put(buf)

	buf.Reset()

	err := json.NewEncoder(buf).Encode(event.Payload)
	if err != nil {
		logger.ErrorKV(ctx, "json marshalling", zap.Error(err))
		return nil
	}

	var m map[string]interface{}
	err = json.NewDecoder(buf).Decode(&m)
	if err != nil {
		logger.ErrorKV(ctx, "json unmarshalling", zap.Error(err))
		return nil
	}

	evt := model.Event{
		ID:        event.EvtID,
		EventKind: event.EvtType,
		CreatedAt: time.UnixMicro(int64(event.Timestamp)),
	}

	var streakTypes []model.StreakType
	for key, val := range m {
		switch jsonSepReplacer.Replace(strings.ToLower(key)) {
		case "appid":
			evt.AppID = swag.String(fmt.Sprint(val))
		case "userid", "authorid":
			evt.UserID = swag.String(fmt.Sprint(val))
			streakTypes = append(streakTypes, model.StreakTypeUser)
		case "circleid":
			evt.CircleID = swag.String(fmt.Sprint(val))
		}
	}

	_, err = s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		err = s.repo.SingleWrite().UpsertEventKind(ctx, event.EvtType)
		if err != nil {
			return errors.WithStack(err)
		}

		err = s.repo.SingleWrite().UpsertEventKindStreakTypes(ctx, evt.EventKind, streakTypes...)
		if err != nil {
			return errors.WithStack(err)
		}

		return s.repo.SingleWrite().UpsertEvent(
			ctx,
			repo.UpsertEventInput{
				ID:        evt.ID,
				AppID:     evt.AppID,
				UserID:    evt.UserID,
				CircleID:  evt.CircleID,
				CreatedAt: evt.CreatedAt,
				EventKind: evt.EventKind,
			},
		)
	})
	if err != nil {
		return err
	}

	streakIDs, err := s.repo.SingleRead().GetStreaksByEventKind(ctx, evt.EventKind)
	if err != nil || len(streakIDs) == 0 {
		return errors.WithStack(err)
	}

	streaks, err := s.repo.SingleRead().GetNonDeletedStreaks(ctx, streakIDs...)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, streak := range streaks {
		err = s.AddEventToStreak(ctx, evt, serialize.Streak(streak))
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) AddEventToStreak(ctx context.Context, evt model.Event, streak model.Streak) error {
	if evt.UserID == nil {
		return nil
	}

	var streakCount int64
	_, err := s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
		us, err := tx.GetLastUserStreak(ctx, streak.ID, *evt.UserID)
		if err != nil {
			return errors.WithStack(err)
		}

		// There are three conditions in which we need to create a new streak:
		//  1. The user hasn't had a streak yet.
		createNewStreak := us == nil
		//  2. Too much time has passed since the last streak.
		createNewStreak = createNewStreak || evt.CreatedAt.After(us.LastBlockEndsAt.Add(us.PeriodDuration))
		//  3. The number of events to complete the last block of the streak was not enough.
		createNewStreak = createNewStreak ||
			(us.LastBlockNumberOfEvents < us.NumberOfEventsPerPeriod && evt.CreatedAt.After(us.LastBlockEndsAt))

		if createNewStreak {
			userStreakID := uuid.NewString()
			err = tx.InsertUserStreak(
				ctx,
				repo.InsertUserStreakInput{
					ID:                      userStreakID,
					UserID:                  *evt.UserID,
					StreakID:                streak.ID,
					PeriodDuration:          streak.PeriodDuration,
					LastBlockNumberOfEvents: 1,
					StartedAt:               evt.CreatedAt,
					LastBlockEndsAt:         evt.CreatedAt.Add(streak.PeriodDuration),
					NumberOfEventsPerPeriod: streak.EventsPerPeriod,
				},
			)
			if err != nil {
				return errors.WithStack(err)
			}

			// To avoid duplication of events in streaks.
			return tx.InsertUserStreakEvent(ctx, evt.ID, streak.ID, userStreakID)
		}

		// If events in pubsub can be repeated several times, then this if should save
		// us from this headache.
		exist, err := tx.ExistEventInUserStreak(ctx, evt.ID, us.ID)
		if err != nil || exist {
			return errors.WithStack(err)
		}

		// To avoid duplication of events in streaks.
		err = tx.InsertUserStreakEvent(ctx, evt.ID, streak.ID, us.ID)
		if err != nil {
			return errors.WithStack(err)
		}

		in := repo.UpdateUserStreakInput{}.WithCount(us.LastBlockNumberOfEvents + 1)
		switch {
		// If the previous block was completed, then start a new block.
		case us.LastBlockNumberOfEvents >= us.NumberOfEventsPerPeriod && evt.CreatedAt.After(us.LastBlockEndsAt):
			in = in.WithCount(1).WithLastEventAt(us.LastBlockEndsAt.Add(us.PeriodDuration))

		// If enough events are filled in the block, we will need to issue coins to the user.
		case *in.LastBlockNumberOfEvents == us.NumberOfEventsPerPeriod:
			streakCount = int64(us.LastBlockEndsAt.Sub(us.StartedAt) / us.PeriodDuration)
		}

		return tx.UpdateUserStreak(ctx, us.ID, in)
	})
	if err != nil {
		return err
	}

	if streakCount == 0 {
		return nil
	}

	streakCoins, err := s.repo.SingleRead().GetStreakCoins(ctx, streak.ID)
	if err != nil {
		return err
	}

	for _, sc := range streakCoins {
		if streakCount%sc.EveryStreakCount == 0 {
			_, err = s.repo.ReadWriteTx(ctx, func(ctx context.Context, tx repo.ReadWriteActions) error {
				transactionID := uuid.NewString()
				err = tx.InsertUserCoinTransaction(ctx, transactionID, *evt.UserID, sc.CoinsCount)
				if err != nil {
					return err
				}

				return tx.UpsertStreakCoinTransaction(ctx, transactionID, streak.ID)
			})
			return err
		}
	}

	return nil
}

var (
	bufPool         sync.Pool
	jsonSepReplacer = strings.NewReplacer("-", "", "_", "")
)
