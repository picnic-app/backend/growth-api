package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/growth-api/internal/util/is"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestDeleteStreakEventKind(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	streak := createStreak(
		t,
		container,
		uuid.NewString(),
		nil,
		1,
		time.Hour,
		event.TypeCommentNew,
	)

	const eventKind = event.PostShared
	initEventKind(t, container, eventKind)

	ctx := adminCtx()
	_, err := container.controller.AddStreakEventKind(
		ctx,
		&v1.AddStreakEventKindRequest{
			StreakId:  streak.GetId(),
			EventKind: eventKind,
		},
	)
	require.NoError(t, err)

	resp, err := container.controller.GetStreakEventKinds(
		ctx,
		&v1.GetStreakEventKindsRequest{
			StreakId: streak.GetId(),
		},
	)
	require.NoError(t, err)
	require.True(t, is.AnyOf(eventKind, resp.GetEventKinds()...), resp.GetEventKinds())

	_, err = container.controller.DeleteStreakEventKind(
		ctx,
		&v1.DeleteStreakEventKindRequest{
			StreakId:  streak.GetId(),
			EventKind: eventKind,
		},
	)
	require.NoError(t, err)

	resp, err = container.controller.GetStreakEventKinds(
		ctx,
		&v1.GetStreakEventKindsRequest{
			StreakId: streak.GetId(),
		},
	)
	require.NoError(t, err)
	require.False(t, is.AnyOf(eventKind, resp.GetEventKinds()...), resp.GetEventKinds())
}

func adminCtx() context.Context {
	return auth.ToAdminCtx(auth.AddUserIDToCtx(context.Background(), uuid.NewString()))
}
