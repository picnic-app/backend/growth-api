package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

// TestAddEventToStreak_StreakGrowing checks that the streak is incremented if
// the streak conditions are completed.
func TestAddEventToStreak_StreakGrowing(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	ctx := auth.AddUserIDToCtx(context.Background(), userID)
	ctx = auth.ToAdminCtx(ctx)

	const eventKind = event.TypeCommentNew
	initEventKind(t, container, eventKind)

	const period = time.Minute
	streak, err := container.service.CreateStreak(
		ctx,
		model.CreateStreakInput{
			Name:            uuid.NewString(),
			StreakType:      model.StreakTypeUser,
			EventsPerPeriod: 2,
			PeriodDuration:  period,
			EventKinds:      []model.EventKind{eventKind},
		},
	)
	require.NoError(t, err)

	coins, err := container.controller.GetUserCoins(ctx, &v1.GetUserCoinsRequest{UserId: userID})
	require.NoError(t, err)
	require.EqualValues(t, 0, coins.GetCoins())

	streakCoins, err := container.controller.GetEarnedStreakCoins(
		ctx,
		&v1.GetEarnedStreakCoinsRequest{
			StreakId: streak.ID,
		},
	)
	require.NoError(t, err)
	require.EqualValues(t, 0, streakCoins.GetCoins(), streakCoins)

	_, err = container.controller.AddStreakCoins(
		ctx,
		&v1.AddStreakCoinsRequest{
			StreakId:         streak.ID,
			CoinsCount:       10,
			EveryStreakCount: 1,
		},
	)
	require.NoError(t, err)

	_, err = container.controller.AddStreakCoins(
		ctx,
		&v1.AddStreakCoinsRequest{
			StreakId:         streak.ID,
			CoinsCount:       20,
			EveryStreakCount: 2,
		},
	)
	require.NoError(t, err)

	now := time.Now().UTC()
	const someTime = period / 5
	for _, evt := range []model.Event{
		{
			// Creates a new streak.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now,
		},
		{
			// Completes condition the new streak for the first period.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(someTime),
		},
		{
			// Starts a new streak block.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(period + someTime),
		},
		{
			// Completes condition of the streak for the second period.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(period + 2*someTime),
		},
	} {
		err = container.spannerRepo.SingleWrite().UpsertEvent(
			ctx,
			repo.UpsertEventInput{
				ID:        evt.ID,
				EventKind: evt.EventKind,
				AppID:     evt.AppID,
				UserID:    evt.UserID,
				CircleID:  evt.CircleID,
				CreatedAt: evt.CreatedAt,
			},
		)
		require.NoError(t, err)

		err = container.service.AddEventToStreak(ctx, evt, streak)
		require.NoError(t, err)
	}

	userStreak, err := container.controller.GetBestUserStreak(
		ctx,
		&v1.GetBestUserStreakRequest{
			StreakId: streak.ID,
		},
	)
	require.NoError(t, err)
	require.EqualValues(t, 2, userStreak.GetUserStreak().GetStreakCount())

	coins, err = container.controller.GetUserCoins(ctx, &v1.GetUserCoinsRequest{UserId: userID})
	require.NoError(t, err)
	require.EqualValues(t, 30, coins.GetCoins())

	streakCoins, err = container.controller.GetEarnedStreakCoins(
		ctx,
		&v1.GetEarnedStreakCoinsRequest{
			StreakId: streak.ID,
		},
	)
	require.NoError(t, err)
	require.EqualValues(t, 30, streakCoins.GetCoins(), streakCoins)
}

// TestAddEventToStreak_NotEnoughEventsInBlockToContinueStreak checks that if
// enough events were not created for the previous period, then the streak
// should be interrupted and a new one should be created.
func TestAddEventToStreak_NotEnoughEventsInBlockToContinueStreak(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	ctx := auth.AddUserIDToCtx(context.Background(), userID)
	ctx = auth.ToAdminCtx(ctx)

	const eventKind = event.TypeCommentNew
	initEventKind(t, container, eventKind)

	const period = time.Minute
	streak, err := container.service.CreateStreak(
		ctx,
		model.CreateStreakInput{
			Name:            uuid.NewString(),
			StreakType:      model.StreakTypeUser,
			EventsPerPeriod: 2,
			PeriodDuration:  period,
			EventKinds:      []model.EventKind{eventKind},
		},
	)
	require.NoError(t, err)

	now := time.Now().UTC()
	const someTime = period / 5
	for _, evt := range []model.Event{
		{
			// Creates a new streak.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now,
		},

		// There is no event that would allow the newly created streak to be completed.

		{
			// Condition of the previous block wasn't completed. Creates a new streak.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(period + someTime),
		},
	} {
		err = container.spannerRepo.SingleWrite().UpsertEvent(
			ctx,
			repo.UpsertEventInput{
				ID:        evt.ID,
				EventKind: evt.EventKind,
				AppID:     evt.AppID,
				UserID:    evt.UserID,
				CircleID:  evt.CircleID,
				CreatedAt: evt.CreatedAt,
			},
		)
		require.NoError(t, err)

		err = container.service.AddEventToStreak(ctx, evt, streak)
		require.NoError(t, err)
	}

	userStreak, err := container.controller.GetCurrentUserStreak(
		ctx,
		&v1.GetCurrentUserStreakRequest{
			StreakId: streak.ID,
		},
	)
	require.NoError(t, err)
	require.EqualValues(t, 0, userStreak.GetUserStreak().GetStreakCount(), userStreak)
}

// TestAddEventToStreak_MissedPeriod checks that if the last streak had a
// complete last block, but then there were no events in the next period, we
// should create a new block.
func TestAddEventToStreak_MissedPeriod(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	ctx := auth.AddUserIDToCtx(context.Background(), userID)
	ctx = auth.ToAdminCtx(ctx)

	const eventKind = event.TypeCommentNew
	initEventKind(t, container, eventKind)

	const period = time.Minute
	streak, err := container.service.CreateStreak(
		ctx,
		model.CreateStreakInput{
			Name:            uuid.NewString(),
			StreakType:      model.StreakTypeUser,
			EventsPerPeriod: 2,
			PeriodDuration:  period,
			EventKinds:      []model.EventKind{eventKind},
		},
	)
	require.NoError(t, err)

	const someTime = period / 5
	now := time.Now().UTC()
	for _, evt := range []model.Event{
		{
			// Creates a new streak.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now,
		},
		{
			// Completes condition of the streak for the period.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(someTime),
		},
		{
			// This event also belongs to the previous period.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(2 * someTime),
		},

		// There are no events here to create a new block to continue the streak.

		{
			// Too much time has passed since the last streak. Creates a new streak.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(2*period + someTime),
		},
		{
			// Completes condition of the new streak for the period.
			ID:        uuid.NewString(),
			EventKind: eventKind,
			UserID:    &userID,
			CreatedAt: now.Add(2*period + 2*someTime),
		},
	} {
		err = container.spannerRepo.SingleWrite().UpsertEvent(
			ctx,
			repo.UpsertEventInput{
				ID:        evt.ID,
				EventKind: evt.EventKind,
				AppID:     evt.AppID,
				UserID:    evt.UserID,
				CircleID:  evt.CircleID,
				CreatedAt: evt.CreatedAt,
			},
		)
		require.NoError(t, err)

		err = container.service.AddEventToStreak(ctx, evt, streak)
		require.NoError(t, err)
	}

	userStreak, err := container.spannerRepo.SingleRead().GetLastUserStreak(ctx, streak.ID, userID)
	require.NoError(t, err)
	require.EqualValues(t, 1, userStreak.CompletedBlocksCount, userStreak)
	require.EqualValues(t, 2, userStreak.LastBlockNumberOfEvents, userStreak)
}

func TestAddEventToStreak_Idempotency(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	ctx := auth.AddUserIDToCtx(context.Background(), userID)
	ctx = auth.ToAdminCtx(ctx)

	const eventKind = event.TypeCommentNew
	initEventKind(t, container, eventKind)

	streak, err := container.service.CreateStreak(
		ctx,
		model.CreateStreakInput{
			Name:            uuid.NewString(),
			StreakType:      model.StreakTypeUser,
			EventsPerPeriod: 2,
			PeriodDuration:  time.Minute,
			EventKinds:      []model.EventKind{eventKind},
		},
	)
	require.NoError(t, err)

	now := time.Now().UTC()
	evt := model.Event{
		ID:        uuid.NewString(),
		EventKind: eventKind,
		UserID:    &userID,
		CreatedAt: now,
	}
	err = container.spannerRepo.SingleWrite().UpsertEvent(
		ctx,
		repo.UpsertEventInput{
			ID:        evt.ID,
			EventKind: evt.EventKind,
			AppID:     evt.AppID,
			UserID:    evt.UserID,
			CircleID:  evt.CircleID,
			CreatedAt: evt.CreatedAt,
		},
	)
	require.NoError(t, err)

	// The same event can come multiple times, we need to ignore it.
	for i := 0; i < 2; i++ {
		err = container.service.AddEventToStreak(ctx, evt, streak)
		require.NoError(t, err)
	}

	userStreak, err := container.spannerRepo.SingleRead().GetLastUserStreak(ctx, streak.ID, userID)
	require.NoError(t, err)
	require.EqualValues(t, 1, userStreak.LastBlockNumberOfEvents)
	require.EqualValues(t, 0, userStreak.CompletedBlocksCount)
}

func initEventKind(t require.TestingT, container Container, eventKind model.EventKind) {
	ctx := context.Background()

	err := container.spannerRepo.SingleWrite().UpsertEventKind(ctx, eventKind)
	require.NoError(t, err)

	err = container.spannerRepo.SingleWrite().UpsertEventKindStreakTypes(ctx, eventKind, model.StreakTypeUser)
	require.NoError(t, err)
}
