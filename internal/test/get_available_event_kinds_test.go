package test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/growth-api/internal/util/is"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestGetAvailableEventKinds(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	const eventKind = event.TypeCommentNew
	initEventKind(t, container, eventKind)

	ctx := adminCtx()
	resp, err := container.controller.GetAvailableEventKinds(ctx, &v1.GetAvailableEventKindsRequest{})
	require.NoError(t, err)
	require.True(t, is.AnyOf(eventKind, resp.GetEventKinds()...), resp.GetEventKinds())
}
