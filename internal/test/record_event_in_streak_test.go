package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestRecordEventInStreak(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	const eventKind = event.TypeNewPost
	initEventKind(t, container, eventKind)

	s := createStreak(t, container, uuid.NewString(), nil, 1, time.Hour, eventKind)

	ctx := auth.ToAdminCtx(auth.AddUserIDToCtx(context.Background(), s.GetCreatedBy()))

	defer func() {
		_, err := container.controller.RemoveStreak(ctx, &v1.RemoveStreakRequest{StreakId: s.GetId()})
		require.NoError(t, err)
	}()

	evt := event.Event{
		EvtID:     uuid.NewString(),
		EvtType:   eventKind,
		Timestamp: uint64(time.Now().UnixMicro()),
		Payload: event.NewPostPayload{
			PostID:       uuid.NewString(),
			UserID:       s.GetCreatedBy(),
			CircleID:     uuid.NewString(),
			CreatedAt:    "",
			Type:         0,
			LanguageCode: "",
			ContentURL:   "",
		},
	}

	err := container.bus.Topic(eventbus.TopicID().Content).Pub(context.Background(), evt)
	require.NoError(t, err)

	cond := func() bool {
		us, err := container.controller.GetCurrentUserStreak(ctx, &v1.GetCurrentUserStreakRequest{StreakId: s.GetId()})
		require.NoError(t, err)

		exist, err := container.spannerRepo.SingleRead().ExistEventInUserStreak(
			ctx,
			evt.EvtID,
			us.GetUserStreak().GetId(),
		)
		require.NoError(t, err)
		return exist
	}
	Eventually(t, cond)
}
