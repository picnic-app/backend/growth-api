package test

import (
	"net/http"
	"testing"
	"time"

	"cloud.google.com/go/spanner"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller"
	spannerRepo "gitlab.com/picnic-app/backend/growth-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/growth-api/internal/service"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	profileV2Mock "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2/mockgen"
)

var (
	bus        eventbus.Client
	spannerCli *spanner.Client
	mux        = &http.ServeMux{}
)

type Container struct {
	mux         *http.ServeMux
	bus         eventbus.Client
	spannerCli  *spanner.Client
	service     *service.Service
	spannerRepo spannerRepo.Repo
	controller  controller.Controller
	mockProfile *profileV2Mock.MockProfileServiceClient
}

func (c Container) Close() {}

func initContainer(t *testing.T, opts ...interface{}) Container {
	ctrl := gomock.NewController(t)

	mockProfile := profileV2Mock.NewMockProfileServiceClient(ctrl)

	repo := spannerRepo.NewRepoWithClient(spannerCli)
	svc := service.New(
		repo,
		mockProfile,
	)

	return Container{
		mux:         mux,
		bus:         bus,
		service:     svc,
		spannerRepo: repo,
		spannerCli:  spannerCli,
		controller:  controller.New(svc),
		mockProfile: mockProfile,
	}
}

const (
	timeout = time.Minute / 2
	tick    = time.Second
)

func Receive[T any](t require.TestingT, ch <-chan T, msgAndArgs ...interface{}) (r T) {
	ticker := time.NewTicker(timeout)
	defer ticker.Stop()

	select {
	case r = <-ch:
	case <-ticker.C:
		require.Fail(t, "timeout has been exceeded", msgAndArgs...)
	}
	return r
}

func Eventually(t require.TestingT, cond func() bool, msgAndArgs ...interface{}) {
	require.Eventually(t, cond, timeout, tick, msgAndArgs...)
}

func Never(t require.TestingT, cond func() bool, msgAndArgs ...interface{}) {
	require.Never(t, cond, 5*time.Second, tick, msgAndArgs...)
}
