package test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/types/known/durationpb"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestUpdateStreak(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	want := createStreak(t, container, uuid.NewString(), nil, 1, time.Hour, event.TypeCommentNew)

	const suffix = "_new"

	want.Name += suffix
	want.Description += suffix
	want.EventsPerPeriod *= 2
	want.Period = durationpb.New(want.GetPeriod().AsDuration() * 2)

	ctx := adminCtx()
	_, err := container.controller.UpdateStreak(
		ctx,
		&v1.UpdateStreakRequest{
			StreakId:        want.GetId(),
			Name:            &want.Name,
			Description:     &want.Description,
			EventsPerPeriod: &want.EventsPerPeriod,
			Period:          want.Period,
		},
	)
	require.NoError(t, err)

	got, err := container.controller.GetStreak(ctx, &v1.GetStreakRequest{Id: want.GetId()})
	require.NoError(t, err)
	require.Equal(t, want, got.GetStreak())
}
