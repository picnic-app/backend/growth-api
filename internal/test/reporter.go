package test

import (
	"context"

	"gitlab.com/picnic-app/backend/libs/golang/logger"
)

type Reporter struct{}

func (r Reporter) Errorf(format string, args ...interface{}) {
	logger.Errorf(context.Background(), format, args...)
}

func (r Reporter) Fatalf(format string, args ...interface{}) {
	logger.Errorf(context.Background(), format, args...)
}
