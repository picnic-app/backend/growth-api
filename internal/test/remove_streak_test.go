package test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestRemoveStreak(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	s := createStreak(t, container, uuid.NewString(), nil, 1, time.Hour, event.TypeCommentNew)

	ctx := adminCtx()
	_, err := container.controller.RemoveStreak(ctx, &v1.RemoveStreakRequest{StreakId: s.GetId()})
	require.NoError(t, err)

	resp, err := container.controller.GetStreaks(ctx, &v1.GetStreaksRequest{Ids: []string{s.GetId()}})
	require.NoError(t, err)
	require.NotContains(t, resp.GetStreaks(), s)
}
