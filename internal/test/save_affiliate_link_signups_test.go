package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestSaveAffiliateLinkSignups(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	userID := uuid.NewString()
	affiliateCodeOwnerID := uuid.NewString()

	t.Run("saves affiliate link signups correctly", func(t *testing.T) {
		err := container.bus.Topic(eventbus.TopicID().Profile).Pub(context.Background(), event.Event{
			EvtID:     uuid.NewString(),
			EvtType:   event.TypeUserNew,
			Timestamp: uint64(time.Now().UnixMicro()),
			Payload: event.NewUserPayload{
				UserID:             userID,
				AffiliateCodeOwner: &affiliateCodeOwnerID,
			},
		})
		require.NoError(t, err)

		var stats *v1.GetAffiliateLinkStatsResponse
		Eventually(t, func() bool {
			stats, err = container.controller.GetAffiliateLinkStats(
				auth.AddUserIDToCtx(context.Background(), affiliateCodeOwnerID),
				&v1.GetAffiliateLinkStatsRequest{
					UserId: affiliateCodeOwnerID,
				},
			)
			if err != nil {
				t.Errorf("When running GetAffiliateLinkStatistics got: err=%v", err)
			}

			return err == nil && stats != nil && stats.GetSignupsCount() == 1
		})

		require.Equal(t, int64(1), stats.GetSignupsCount())
		require.Equal(t, int64(0), stats.GetClicksCount())
	})

	t.Run("ignores duplicate signups", func(t *testing.T) {
		err := container.bus.Topic(eventbus.TopicID().Profile).Pub(context.Background(), event.Event{
			EvtID:     uuid.NewString(),
			EvtType:   event.TypeUserNew,
			Timestamp: uint64(time.Now().UnixMicro()),
			Payload: event.NewUserPayload{
				UserID:             userID,
				AffiliateCodeOwner: &affiliateCodeOwnerID,
			},
		})
		require.NoError(t, err)

		var stats *v1.GetAffiliateLinkStatsResponse

		Never(t, func() bool {
			stats, err = container.controller.GetAffiliateLinkStats(
				auth.AddUserIDToCtx(context.Background(), affiliateCodeOwnerID),
				&v1.GetAffiliateLinkStatsRequest{
					UserId: affiliateCodeOwnerID,
				},
			)

			return err == nil && stats != nil && stats.GetSignupsCount() > 1
		})

		require.Equal(t, int64(1), stats.GetSignupsCount())
		require.Equal(t, int64(0), stats.GetClicksCount())
	})
}
