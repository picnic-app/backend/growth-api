//go:build !unit

package test

import (
	"context"
	"log"
	"os"
	"strings"
	"testing"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/pubsub/pstest"
	"cloud.google.com/go/spanner"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"google.golang.org/api/option"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	spannerRepo "gitlab.com/picnic-app/backend/growth-api/internal/repo/spanner"
	"gitlab.com/picnic-app/backend/growth-api/internal/service"
	"gitlab.com/picnic-app/backend/libs/golang/config"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	"gitlab.com/picnic-app/backend/libs/golang/monitoring/tracing"
	profileV2Mock "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2/mockgen"
)

func TestMain(m *testing.M) {
	var code int
	defer func() { os.Exit(code) }()

	// To avoid data races.
	tracing.DefaultTracer = tracing.GetTracer(config.ServiceName())

	// Change the working dir to the root dir
	if err := os.Chdir("./../.."); err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()

	if isCI() {
		os.Setenv("APP_ENV", "dev.yaml")
		defer os.Unsetenv("APP_ENV")
	} else {
		os.Setenv("SPANNER_EMULATOR_HOST", "localhost:9010")
		defer os.Unsetenv("SPANNER_EMULATOR_HOST")
	}

	cfg, err := config.GetConfig(ctx)
	if err != nil {
		log.Fatal("failed to load config: ", err)
	}

	spannerCli, err = spannerRepo.NewSpannerClient(ctx, cfg.Spanner.DSN, cfg.Spanner.ADCPath)
	if err != nil {
		log.Fatal("failed to create spanner client: ", err)
	}
	defer spannerCli.Close()

	logger.Info(ctx, "Checking connection to spanner...")
	row := spannerCli.Single().Query(ctx, spanner.Statement{SQL: "SELECT 1;"})
	err = row.Do(func(r *spanner.Row) error { return nil })
	if err != nil {
		log.Fatal("failed to execute query with spanner client: ", err)
	}

	srv := pstest.NewServer()
	defer srv.Close()

	conn, err := grpc.Dial(srv.Addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("failed to dial connection: ", err)
	}
	defer conn.Close()

	client, err := pubsub.NewClient(ctx, cfg.Pubsub.Project, option.WithGRPCConn(conn))
	if err != nil {
		log.Fatal("failed to create pubsub client: ", err)
	}
	defer client.Close()

	err = initPubSubTopics(ctx, client, cfg.ServiceName, cfg.EnvID)
	if err != nil {
		log.Fatal("init pub sub topics: ", err)
	}

	bus = eventbus.NewWithPubSubClientClient(client, cfg.Pubsub.Project, cfg.ServiceName, cfg.EnvID)
	defer bus.Close()

	ctrl := gomock.NewController(Reporter{})

	svc := service.New(
		spannerRepo.NewRepoWithClient(spannerCli),
		profileV2Mock.NewMockProfileServiceClient(ctrl),
	)

	err = svc.InitRepo(ctx)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	err = svc.ListenEvents(ctx, bus)
	if err != nil {
		logger.Fatal(ctx, err)
	}

	code = m.Run()
}

func initPubSubTopics(ctx context.Context, cli *pubsub.Client, serviceName, envID string) (err error) {
	for _, topicID := range []string{
		eventbus.TopicID().App,
		eventbus.TopicID().Chat,
		eventbus.TopicID().Circle,
		eventbus.TopicID().Content,
		eventbus.TopicID().Comment,
		eventbus.TopicID().Profile,
	} {
		err = createTopic(ctx, cli, topicID, serviceName, envID)
		if err != nil {
			return errors.Wrapf(err, "creating topic %q", topicID)
		}
	}
	return nil
}

func createTopic(ctx context.Context, cli *pubsub.Client, topicID, serviceName, envID string) error {
	topic, err := cli.CreateTopic(ctx, topicID)
	if err != nil {
		return errors.WithStack(err)
	}

	subID := strings.Join([]string{topicID, serviceName, envID}, ".")
	_, err = cli.CreateSubscription(ctx, subID, pubsub.SubscriptionConfig{Topic: topic})
	return errors.WithStack(err)
}

func isCI() bool {
	value, found := os.LookupEnv("CI")
	if !found {
		return false
	}
	return strings.ToLower(value) != "false"
}
