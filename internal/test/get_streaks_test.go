package test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestGetStreaks(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	want := createStreak(t, container, uuid.NewString(), nil, 1, time.Hour, event.TypeCommentNew)

	ctx := adminCtx()
	got, err := container.controller.GetStreaks(ctx, &v1.GetStreaksRequest{Ids: []string{want.GetId()}})
	require.NoError(t, err)
	require.Contains(t, got.GetStreaks(), want)
}
