package test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/types/known/durationpb"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestCreateStreak(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	want := createStreak(
		t,
		container,
		uuid.NewString(),
		nil,
		1,
		24*time.Hour,
		event.TypeCommentNew,
	)

	ctx := auth.AddUserIDToCtx(context.Background(), want.GetCreatedBy())
	got, err := container.controller.GetStreak(ctx, &v1.GetStreakRequest{Id: want.GetId()})
	require.NoError(t, err)
	require.Equal(t, want, got.GetStreak())
}

func createStreak(
	t require.TestingT,
	container Container,
	name string,
	description *string,
	eventsPerPeriod int64,
	period time.Duration,
	eventKinds ...string,
) *v1.Streak {
	userID := uuid.NewString()
	ctx := auth.AddUserIDToCtx(context.Background(), userID)
	ctx = auth.ToAdminCtx(ctx)

	for _, kind := range eventKinds {
		initEventKind(t, container, kind)
	}

	resp, err := container.controller.CreateStreak(
		ctx,
		&v1.CreateStreakRequest{
			Name:            name,
			Description:     description,
			EventsPerPeriod: eventsPerPeriod,
			Period:          durationpb.New(period),
			EventKinds:      eventKinds,
		},
	)
	require.NoError(t, err)
	return resp.GetStreak()
}
