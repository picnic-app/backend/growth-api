package test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/core/auth"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
	v2 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/profile-api/profile/v2"
)

func TestSaveAffiliateLinkMobileAppDownloads(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	t.Run("returns error when code is empty", func(t *testing.T) {
		_, err := container.controller.SaveAffiliateLinkMobileAppDownload(
			context.Background(),
			&v1.SaveAffiliateLinkMobileAppDownloadRequest{
				Code: "",
			})
		require.EqualError(t, err, "rpc error: code = InvalidArgument desc = parameter \"affiliate code\" required")
	})

	code := uuid.NewString()[0:8]
	userID := uuid.NewString()

	t.Run("saves affiliate link mobile app downloads correctly", func(t *testing.T) {
		container.mockProfile.
			EXPECT().
			GetUserIDByAffiliateCodeWithoutAuth(gomock.Any(), &v2.GetUserIDByAffiliateCodeWithoutAuthRequest{
				AffiliateCode: code,
			}).
			Return(&v2.GetUserIDByAffiliateCodeWithoutAuthResponse{
				UserId: userID,
			}, nil).
			Times(1)

		stats, err := container.controller.GetAffiliateLinkStats(
			auth.AddUserIDToCtx(context.Background(), uuid.NewString()),
			&v1.GetAffiliateLinkStatsRequest{
				UserId: userID,
			},
		)
		require.NoError(t, err)
		require.Equal(t, 0, int(stats.GetClicksCount()))
		require.Equal(t, 0, int(stats.GetSignupsCount()))
		require.Equal(t, 0, int(stats.GetMobileAppDownloadsCount()))

		_, err = container.controller.SaveAffiliateLinkMobileAppDownload(
			context.Background(),
			&v1.SaveAffiliateLinkMobileAppDownloadRequest{
				Code: code,
			},
		)
		require.NoError(t, err)

		stats, err = container.controller.GetAffiliateLinkStats(
			auth.AddUserIDToCtx(context.Background(), uuid.NewString()),
			&v1.GetAffiliateLinkStatsRequest{
				UserId: userID,
			},
		)
		require.NoError(t, err)
		require.Equal(t, 0, int(stats.GetClicksCount()))
		require.Equal(t, 0, int(stats.GetSignupsCount()))
		require.Equal(t, 1, int(stats.GetMobileAppDownloadsCount()))
	})
}
