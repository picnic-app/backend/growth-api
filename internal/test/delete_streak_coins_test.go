package test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.com/picnic-app/backend/libs/golang/eventbus/event"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func TestDeleteStreakCoins(t *testing.T) {
	t.Parallel()

	container := initContainer(t)
	defer container.Close()

	s := createStreak(t, container, uuid.NewString(), nil, 1, time.Hour, event.TypeCommentNew)

	ctx := adminCtx()
	_, err := container.controller.AddStreakCoins(
		ctx,
		&v1.AddStreakCoinsRequest{
			StreakId:         s.GetId(),
			CoinsCount:       1,
			EveryStreakCount: 10,
		},
	)
	require.NoError(t, err)

	_, err = container.controller.DeleteStreakCoins(
		ctx,
		&v1.DeleteStreakCoinsRequest{
			StreakId:         s.GetId(),
			EveryStreakCount: 10,
		},
	)
	require.NoError(t, err)

	resp, err := container.controller.GetStreakCoins(
		ctx,
		&v1.GetStreakCoinsRequest{
			StreakId: s.GetId(),
		},
	)
	require.NoError(t, err)
	require.Len(t, resp.GetStreakCoins(), 0)
}
