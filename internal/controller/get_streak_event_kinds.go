package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetStreakEventKinds(
	ctx context.Context,
	req *v1.GetStreakEventKindsRequest,
) (*v1.GetStreakEventKindsResponse, error) {
	eventKinds, err := c.service.GetStreakEventKinds(ctx, req.GetStreakId())
	return &v1.GetStreakEventKindsResponse{EventKinds: eventKinds}, err
}
