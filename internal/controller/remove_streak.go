package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) RemoveStreak(ctx context.Context, req *v1.RemoveStreakRequest) (*v1.RemoveStreakResponse, error) {
	err := c.service.RemoveStreak(ctx, req.GetStreakId())
	return &v1.RemoveStreakResponse{}, err
}
