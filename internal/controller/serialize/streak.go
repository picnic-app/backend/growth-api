package serialize

import (
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func Streak(s model.Streak) *v1.Streak {
	return &v1.Streak{
		Id:              s.ID,
		Name:            s.Name,
		CreatedBy:       s.CreatedBy,
		Description:     s.Description,
		EventsPerPeriod: s.EventsPerPeriod,
		Period:          durationpb.New(s.PeriodDuration),
		CreatedAt:       timestamppb.New(s.CreatedAt),
	}
}
