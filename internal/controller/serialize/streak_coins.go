package serialize

import (
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func StreakCoins(sc model.StreakCoin) *v1.StreakCoin {
	return &v1.StreakCoin{
		CoinsCount:       sc.CoinsCount,
		EveryStreakCount: sc.EveryStreakCount,
	}
}
