package serialize

import (
	"google.golang.org/protobuf/types/known/durationpb"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func UserStreak(s model.UserStreak) *v1.UserStreak {
	return &v1.UserStreak{
		Id:                     s.ID,
		UserId:                 s.UserID,
		StreakId:               s.StreakID,
		StreakCount:            s.CompletedBlocksCount,
		RequiredCountOfEvents:  s.NumberOfEventsPerPeriod,
		LastBlockCountOfEvents: s.LastBlockNumberOfEvents,
		Period:                 durationpb.New(s.PeriodDuration),
	}
}

func Pointer[A, B any](v *A, f func(A) *B) *B {
	if v == nil {
		return nil
	}
	return f(*v)
}
