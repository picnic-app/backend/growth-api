package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetBestUserStreak(
	ctx context.Context,
	req *v1.GetBestUserStreakRequest,
) (*v1.GetBestUserStreakResponse, error) {
	us, err := c.service.GetBestUserStreak(ctx, req.GetStreakId())
	return &v1.GetBestUserStreakResponse{UserStreak: serialize.Pointer(us, serialize.UserStreak)}, err
}
