package controller

import (
	"google.golang.org/grpc"

	"gitlab.com/picnic-app/backend/growth-api/internal/service"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func New(svc *service.Service) Controller {
	return Controller{
		service: svc,
	}
}

// Controller represents struct that implements GrowthServiceServer.
type Controller struct {
	service *service.Service

	v1.UnimplementedGrowthServiceServer
}

func (c Controller) Register(reg grpc.ServiceRegistrar) {
	v1.RegisterGrowthServiceServer(reg, c)
}
