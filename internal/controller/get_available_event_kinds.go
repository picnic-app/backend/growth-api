package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetAvailableEventKinds(
	ctx context.Context,
	_ *v1.GetAvailableEventKindsRequest,
) (*v1.GetAvailableEventKindsResponse, error) {
	eventKinds, err := c.service.GetEventKindsByStreakType(ctx, model.StreakTypeUser)
	return &v1.GetAvailableEventKindsResponse{EventKinds: eventKinds}, err
}
