package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetEarnedStreakCoins(
	ctx context.Context,
	req *v1.GetEarnedStreakCoinsRequest,
) (*v1.GetEarnedStreakCoinsResponse, error) {
	coins, err := c.service.GetEarnedStreakCoins(ctx, req.GetStreakId())
	return &v1.GetEarnedStreakCoinsResponse{Coins: coins}, err
}
