package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetUserCoins(ctx context.Context, req *v1.GetUserCoinsRequest) (*v1.GetUserCoinsResponse, error) {
	coins, err := c.service.GetUserCoins(ctx, req.GetUserId())
	return &v1.GetUserCoinsResponse{Coins: coins}, err
}
