package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetStreak(ctx context.Context, req *v1.GetStreakRequest) (*v1.GetStreakResponse, error) {
	streak, err := c.service.GetStreak(ctx, req.GetId())
	return &v1.GetStreakResponse{Streak: serialize.Streak(streak)}, err
}
