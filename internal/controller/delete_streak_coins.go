package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) DeleteStreakCoins(
	ctx context.Context,
	req *v1.DeleteStreakCoinsRequest,
) (*v1.DeleteStreakCoinsResponse, error) {
	err := c.service.DeleteStreakCoins(ctx, req.GetStreakId(), req.GetEveryStreakCount())
	return &v1.DeleteStreakCoinsResponse{}, err
}
