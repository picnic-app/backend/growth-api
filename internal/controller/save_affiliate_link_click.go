package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) SaveAffiliateLinkClick(
	ctx context.Context,
	req *v1.SaveAffiliateLinkClickRequest,
) (*v1.SaveAffiliateLinkClickResponse, error) {
	err := c.service.SaveAffiliateLinkClick(
		ctx,
		req.GetCode(),
		req.GetFingerprint(),
	)
	if err != nil {
		return nil, err
	}

	return &v1.SaveAffiliateLinkClickResponse{}, nil
}
