package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetCurrentUserStreak(
	ctx context.Context,
	req *v1.GetCurrentUserStreakRequest,
) (*v1.GetCurrentUserStreakResponse, error) {
	us, err := c.service.GetCurrentUserStreak(ctx, req.GetStreakId())
	return &v1.GetCurrentUserStreakResponse{UserStreak: serialize.Pointer(us, serialize.UserStreak)}, err
}
