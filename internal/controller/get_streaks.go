package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/slice"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetStreaks(ctx context.Context, req *v1.GetStreaksRequest) (*v1.GetStreaksResponse, error) {
	streaks, err := c.service.GetStreaks(ctx, req.GetIds()...)
	return &v1.GetStreaksResponse{Streaks: slice.Convert(serialize.Streak, streaks...)}, err
}
