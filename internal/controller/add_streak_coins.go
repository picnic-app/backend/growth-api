package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) AddStreakCoins(
	ctx context.Context,
	req *v1.AddStreakCoinsRequest,
) (*v1.AddStreakCoinsResponse, error) {
	err := c.service.AddStreakCoins(ctx, req.GetStreakId(), req.GetCoinsCount(), req.GetEveryStreakCount())
	return &v1.AddStreakCoinsResponse{}, err
}
