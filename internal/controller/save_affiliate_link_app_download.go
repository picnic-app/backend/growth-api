package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) SaveAffiliateLinkMobileAppDownload(
	ctx context.Context,
	req *v1.SaveAffiliateLinkMobileAppDownloadRequest,
) (*v1.SaveAffiliateLinkMobileAppDownloadResponse, error) {
	err := c.service.SaveAffiliateLinkMobileAppDownload(
		ctx,
		req.GetCode(),
	)
	if err != nil {
		return nil, err
	}

	return &v1.SaveAffiliateLinkMobileAppDownloadResponse{}, nil
}
