package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/util/slice"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetStreakCoins(
	ctx context.Context,
	req *v1.GetStreakCoinsRequest,
) (*v1.GetStreakCoinsResponse, error) {
	sc, err := c.service.GetStreakCoins(ctx, req.GetStreakId())
	return &v1.GetStreakCoinsResponse{StreakCoins: slice.Convert(serialize.StreakCoins, sc...)}, err
}
