package deserialize

import (
	"time"

	"google.golang.org/protobuf/types/known/durationpb"
)

func Duration(d *durationpb.Duration) *time.Duration {
	if d == nil {
		return nil
	}

	out := d.AsDuration()
	return &out
}
