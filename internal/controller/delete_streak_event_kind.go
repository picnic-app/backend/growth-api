package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) DeleteStreakEventKind(
	ctx context.Context,
	req *v1.DeleteStreakEventKindRequest,
) (*v1.DeleteStreakEventKindResponse, error) {
	err := c.service.DeleteStreakEventKind(ctx, req.GetStreakId(), req.GetEventKind())
	return &v1.DeleteStreakEventKindResponse{}, err
}
