package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) AddStreakEventKind(
	ctx context.Context,
	req *v1.AddStreakEventKindRequest,
) (*v1.AddStreakEventKindResponse, error) {
	err := c.service.AddStreakEventKind(ctx, req.GetStreakId(), req.GetEventKind())
	return &v1.AddStreakEventKindResponse{}, err
}
