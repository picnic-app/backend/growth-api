package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/serialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) CreateStreak(ctx context.Context, req *v1.CreateStreakRequest) (*v1.CreateStreakResponse, error) {
	streak, err := c.service.CreateStreak(
		ctx,
		model.CreateStreakInput{
			Name:            req.GetName(),
			Description:     req.GetDescription(),
			StreakType:      model.StreakTypeUser,
			EventsPerPeriod: req.GetEventsPerPeriod(),
			PeriodDuration:  req.GetPeriod().AsDuration(),
			EventKinds:      req.GetEventKinds(),
		},
	)
	return &v1.CreateStreakResponse{Streak: serialize.Streak(streak)}, err
}
