package controller

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/controller/deserialize"
	"gitlab.com/picnic-app/backend/growth-api/internal/model"
	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) UpdateStreak(ctx context.Context, req *v1.UpdateStreakRequest) (*v1.UpdateStreakResponse, error) {
	err := c.service.UpdateStreak(
		ctx,
		req.GetStreakId(),
		model.UpdateStreakInput{
			Name:            req.Name,
			Description:     req.Description,
			EventsPerPeriod: req.EventsPerPeriod,
			PeriodDuration:  deserialize.Duration(req.Period),
		},
	)
	return &v1.UpdateStreakResponse{}, err
}
