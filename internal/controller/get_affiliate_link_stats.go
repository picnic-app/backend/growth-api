package controller

import (
	"context"

	v1 "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/growth-api/growth/v1"
)

func (c Controller) GetAffiliateLinkStats(
	ctx context.Context,
	req *v1.GetAffiliateLinkStatsRequest,
) (*v1.GetAffiliateLinkStatsResponse, error) {
	stats, err := c.service.GetAffiliateLinkStatistics(
		ctx,
		req.UserId,
	)
	if err != nil {
		return nil, err
	}

	return &v1.GetAffiliateLinkStatsResponse{
		ClicksCount:             stats.Clicks,
		SignupsCount:            stats.Signups,
		MobileAppDownloadsCount: stats.MobileAppDownloads,
	}, nil
}
