package repo

import "time"

type UpsertEventInput struct {
	ID        string
	EventKind EventKind
	AppID     *string
	UserID    *string
	CircleID  *string
	CreatedAt time.Time
}

type Event struct {
	ID        string
	EventKind EventKind
	AppID     *string
	UserID    *string
	CircleID  *string
	CreatedAt time.Time
}
