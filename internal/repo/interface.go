package repo

import (
	"context"
	"time"

	"github.com/pkg/errors"
)

var ErrNotFound = errors.New("not found")

type ReadActions interface {
	GetUserCoins(context.Context, UserID) (int64, error)
	GetStreaks(context.Context, ...ID) ([]Streak, error)
	GetNonDeletedStreaks(context.Context, ...ID) ([]Streak, error)
	GetStreaksByEventKind(context.Context, EventKind) ([]ID, error)
	GetStreakCoins(context.Context, StreakID) ([]StreakCoins, error)
	GetStreakEventKinds(context.Context, StreakID) ([]EventKind, error)
	GetUserStreakCoins(context.Context, StreakID, UserID) (int64, error)
	GetLastUserStreak(context.Context, StreakID, UserID) (*UserStreak, error)
	GetBestStreakOfUser(context.Context, StreakID, UserID) (*UserStreak, error)
	GetEventKindsByStreakType(context.Context, StreakType) ([]EventKind, error)
	ExistEventInUserStreak(context.Context, EventID, UserStreakID) (bool, error)
	ExistsAffiliateLinkClick(ctx context.Context, ownerID UserID, fingerprint DeviceFingerprint) (bool, error)
	ExistsAffiliateLinkSignup(ctx context.Context, ownerID, userID UserID) (bool, error)
	// GetAffiliateLinkStatistics Returns repo.ErrNotFound if there is no statistics for the user
	GetAffiliateLinkStatistics(ctx context.Context, ownerID UserID) (AffiliateLinkStatistics, error)
}

type WriteActions interface {
	UpsertEventKind(context.Context, EventKind) error
	UpsertEvent(context.Context, UpsertEventInput) error
	RemoveStreak(context.Context, StreakID, UserID) error
	InsertStreak(context.Context, InsertStreakInput) error
	UpdateStreak(context.Context, UpdateStreakInput) error
	InsertUserStreak(context.Context, InsertUserStreakInput) error
	DeleteStreakEventKind(context.Context, StreakID, EventKind) error
	UpsertStreakEventKinds(context.Context, StreakID, ...EventKind) error
	UpsertStreakType(ctx context.Context, st StreakType, name string) error
	UpsertEventKindStreakTypes(context.Context, EventKind, ...StreakType) error
	UpsertStreakCoinTransaction(context.Context, TransactionID, StreakID) error
	UpdateUserStreak(context.Context, UserStreakID, UpdateUserStreakInput) error
	InsertUserStreakEvent(context.Context, EventID, StreakID, UserStreakID) error
	InsertUserCoinTransaction(ctx context.Context, id TransactionID, userIDs UserID, amount int64) error
	DeleteStreakCoins(ctx context.Context, streakID StreakID, everyStreakCount int64) error
	UpsertStreakCoins(ctx context.Context, streakID StreakID, coinsCount, everyStreakCount int64) error
	InsertAffiliateLinkClick(ctx context.Context, ownerID, fingerprint DeviceFingerprint) error
	InsertAffiliateLinkSignup(ctx context.Context, ownerID, userID UserID) error
	UpsertAffiliateLinkStatistics(ctx context.Context, statistics AffiliateLinkStatistics) error
}

type ReadWriteActions interface {
	ReadActions
	WriteActions
}

type Repo interface {
	SingleRead() ReadActions
	SingleWrite() WriteActions
	ReadOnlyTx(context.Context, func(ctx context.Context, tx ReadActions) error) error
	ReadWriteTx(context.Context, func(ctx context.Context, tx ReadWriteActions) error) (time.Time, error)
}
