package repo

import "time"

type InsertUserStreakInput struct {
	ID                      string
	UserID                  string
	StreakID                string
	LastBlockEndsAt         time.Time
	StartedAt               time.Time
	PeriodDuration          time.Duration
	LastBlockNumberOfEvents int64
	NumberOfEventsPerPeriod int64
}

type UpdateUserStreakInput struct {
	LastBlockNumberOfEvents *int64
	LastBlockEndsAt         *time.Time
}

func (i UpdateUserStreakInput) WithCount(v int64) UpdateUserStreakInput {
	i.LastBlockNumberOfEvents = &v
	return i
}

func (i UpdateUserStreakInput) WithLastEventAt(v time.Time) UpdateUserStreakInput {
	i.LastBlockEndsAt = &v
	return i
}

type UserStreak struct {
	ID       string
	UserID   string
	StreakID string
	// StartedAt time of the first event in the string.
	StartedAt time.Time
	// LastBlockEndsAt time before which events must occur for the block to be
	// considered completed.
	LastBlockEndsAt time.Time
	// CompletedBlocksCount number of completed streak blocks.
	CompletedBlocksCount int64
	// LastBlockNumberOfEvents number of events that occurred in the last block.
	LastBlockNumberOfEvents int64
	// NumberOfEventsPerPeriod number of events in the block required to complete
	// the block.
	NumberOfEventsPerPeriod int64
	// PeriodDuration block duration specified by the streak configuration.
	PeriodDuration time.Duration
	// StreakLastsDuration duration of this streak, from StartedAt to
	// LastBlockEndsAt.
	StreakLastsDuration time.Duration
}
