package repo

type StreakCoins struct {
	StreakID         StreakID
	CoinsCount       int64
	EveryStreakCount int64
}
