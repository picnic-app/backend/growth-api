// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) AffiliateLinkClicks() AffiliateLinkClicks { return AffiliateLinkClicks(t) }

type AffiliateLinkClicks struct{ alias string }

func (t AffiliateLinkClicks) TableAlias() string { return t.alias }
func (t AffiliateLinkClicks) name() string       { return "AffiliateLinkClicks" }
func (t AffiliateLinkClicks) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t AffiliateLinkClicks) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.OwnerID(),
		c.CreatedAt(),
		c.DeviceFingerprint(),
	}
}

func (t AffiliateLinkClicks) Columns() AffiliateLinkClicksCols {
	return AffiliateLinkClicksCols{tableAlias: t.alias}
}

type AffiliateLinkClicksCols struct{ tableAlias string }

func (c AffiliateLinkClicksCols) OwnerID() string   { return colName(c.tableAlias, "OwnerID") }   // STRING(36)
func (c AffiliateLinkClicksCols) CreatedAt() string { return colName(c.tableAlias, "CreatedAt") } // TIMESTAMP OPTIONS (allow_commit_timestamp = true)
func (c AffiliateLinkClicksCols) DeviceFingerprint() string {
	return colName(c.tableAlias, "DeviceFingerprint")
} // STRING(255)
