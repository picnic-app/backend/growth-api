// Code generated by tablegen. DO NOT EDIT.
package tables

func (t AllTables) StreakTypes() StreakTypes { return StreakTypes(t) }

type StreakTypes struct{ alias string }

func (t StreakTypes) TableAlias() string { return t.alias }
func (t StreakTypes) name() string       { return "StreakTypes" }
func (t StreakTypes) TableName() string  { return tableName(t.name(), "", t.alias) }
func (t StreakTypes) AllColumnNames() []string {
	c := t.Columns()
	return []string{
		c.Name(),
		c.StreakType(),
	}
}

func (t StreakTypes) Columns() StreakTypesCols { return StreakTypesCols{tableAlias: t.alias} }

type StreakTypesCols struct{ tableAlias string }

func (c StreakTypesCols) Name() string       { return colName(c.tableAlias, "Name") }       // STRING(20)
func (c StreakTypesCols) StreakType() string { return colName(c.tableAlias, "StreakType") } // INT64
