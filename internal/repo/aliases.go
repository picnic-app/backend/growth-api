package repo

type (
	ID                = string
	UserID            = string
	EventID           = string
	StreakID          = string
	UserStreakID      = string
	TransactionID     = string
	EventKind         = string
	StreakType        = int64
	DeviceFingerprint = string
)
