package repo

import "time"

type InsertStreakInput struct {
	ID              string
	Name            string
	CreatedBy       string
	Description     string
	StreakType      StreakType
	EventsPerPeriod int64
	PeriodDuration  time.Duration
}

type UpdateStreakInput struct {
	StreakID        string
	UserID          string
	Name            *string
	Description     *string
	EventsPerPeriod *int64
	PeriodDuration  *time.Duration
}

type Streak struct {
	ID              string
	Name            string
	CreatedBy       string
	Description     string
	StreakType      StreakType
	EventsPerPeriod int64
	PeriodDuration  time.Duration
	CreatedAt       time.Time
	DeletedAt       *time.Time
	DeletedBy       *string
	UpdatedAt       *time.Time
	UpdatedBy       *string
}
