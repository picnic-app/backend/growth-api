package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertAffiliateLinkSignup(
	ctx context.Context,
	ownerID,
	userID repo.UserID,
) error {
	return InsertAffiliateLinkSignup(ctx, w.bufferWriter(ctx), ownerID, userID)
}

func (rw readWrite) InsertAffiliateLinkSignup(
	ctx context.Context,
	ownerID,
	userID repo.UserID,
) error {
	return InsertAffiliateLinkSignup(ctx, rw.tx, ownerID, userID)
}

func InsertAffiliateLinkSignup(
	_ context.Context,
	db BufferWriter,
	ownerID,
	userID repo.UserID,
) error {
	table := tables.Get().AffiliateLinkSignups()
	columns := table.Columns()
	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			columns.OwnerID():   ownerID,
			columns.UserID():    userID,
			columns.CreatedAt(): spanner.CommitTimestamp,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
