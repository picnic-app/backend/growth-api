package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertStreakCoinTransaction(ctx context.Context, transactionID, streakID string) error {
	return UpsertStreakCoinTransaction(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), transactionID, streakID)
}

func (rw readWrite) UpsertStreakCoinTransaction(ctx context.Context, transactionID, streakID string) error {
	return UpsertStreakCoinTransaction(ctx, rw.tx, transactionID, streakID)
}

func UpsertStreakCoinTransaction(_ context.Context, db BufferWriter, transactionID, streakID string) error {
	table := tables.Get().StreakCoinTransactions()

	op := spanner.InsertOrUpdate(
		table.TableName(),
		Cols{table.Columns().ID(), table.Columns().StreakID()},
		Vals{transactionID, streakID},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
