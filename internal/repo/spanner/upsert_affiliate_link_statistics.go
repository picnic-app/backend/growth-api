package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertAffiliateLinkStatistics(
	ctx context.Context,
	statistics repo.AffiliateLinkStatistics,
) error {
	return UpsertAffiliateLinkStatistics(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), statistics)
}

func (rw readWrite) UpsertAffiliateLinkStatistics(
	ctx context.Context,
	statistics repo.AffiliateLinkStatistics,
) error {
	return UpsertAffiliateLinkStatistics(ctx, rw.tx, statistics)
}

func UpsertAffiliateLinkStatistics(
	_ context.Context,
	db BufferWriter,
	statistics repo.AffiliateLinkStatistics,
) error {
	table := tables.Get().AffiliateLinksStatistics()
	columns := table.Columns()

	op := spanner.InsertOrUpdate(
		table.TableName(),
		Cols{
			columns.OwnerID(),
			columns.Clicks(),
			columns.Signups(),
			columns.MobileAppDownloads(),
		},
		Vals{
			statistics.OwnerID,
			statistics.Clicks,
			statistics.Signups,
			statistics.MobileAppDownloads,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
