package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetBestStreakOfUser(ctx context.Context, streakID, userID string) (*repo.UserStreak, error) {
	return GetBestStreakOfUser(ctx, r.tx, streakID, userID)
}

func (rw readWrite) GetBestStreakOfUser(ctx context.Context, streakID, userID string) (*repo.UserStreak, error) {
	return GetBestStreakOfUser(ctx, rw.tx, streakID, userID)
}

func GetBestStreakOfUser(ctx context.Context, db Reader, streakID, userID string) (*repo.UserStreak, error) {
	index := tables.Get().UserStreaks().Indexes().ByStreakIDUserIDCompletedBlocksCountDesc()
	key := spanner.Key{streakID, userID}.AsPrefix()
	opts := spanner.ReadOptions{
		Limit:      1,
		Index:      index.Name(),
		RequestTag: "GetBestStreakOfUser",
	}
	iter := db.ReadWithOptions(ctx, index.Table(), key, index.AllColumnNames(), &opts)
	return GetResultPtr[repo.UserStreak](iter)
}
