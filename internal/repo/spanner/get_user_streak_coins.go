package spanner

import (
	"context"

	"github.com/Masterminds/squirrel"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetUserStreakCoins(ctx context.Context, streakID, userID string) (int64, error) {
	return GetUserStreakCoins(ctx, r.tx, streakID, userID)
}

func (rw readWrite) GetUserStreakCoins(ctx context.Context, streakID, userID string) (int64, error) {
	return GetUserStreakCoins(ctx, rw.tx, streakID, userID)
}

func GetUserStreakCoins(ctx context.Context, db Reader, streakID, userID string) (int64, error) {
	coins := tables.WithAlias("c").UserCoinTransactions()
	streaks := tables.WithAlias("s").StreakCoinTransactions()

	builder := squirrel.
		Select("COALESCE(SUM(" + coins.Columns().Amount() + "), 0)").
		From(coins.Indexes().ByUserIDCreatedAt().ForcedTable()).
		InnerJoin(streaks.TableName() + " ON " + streaks.Columns().ID() + " = " + coins.Columns().ID()).
		Where(squirrel.Eq{coins.Columns().UserID(): userID, streaks.Columns().StreakID(): streakID})

	return GetResultByBuilder[int64](ctx, db, builder)
}
