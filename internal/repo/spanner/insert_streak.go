package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertStreak(ctx context.Context, in repo.InsertStreakInput) error {
	return InsertStreak(ctx, w.bufferWriter(ctx), in)
}

func (rw readWrite) InsertStreak(ctx context.Context, in repo.InsertStreakInput) error {
	return InsertStreak(ctx, rw.tx, in)
}

func InsertStreak(_ context.Context, db BufferWriter, in repo.InsertStreakInput) error {
	table := tables.Get().Streaks()
	columns := table.Columns()
	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			columns.ID():              in.ID,
			columns.Name():            in.Name,
			columns.CreatedBy():       in.CreatedBy,
			columns.StreakType():      in.StreakType,
			columns.Description():     in.Description,
			columns.PeriodDuration():  in.PeriodDuration,
			columns.EventsPerPeriod(): in.EventsPerPeriod,
			columns.CreatedAt():       spanner.CommitTimestamp,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
