package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetLastUserStreak(ctx context.Context, streakID, userID string) (*repo.UserStreak, error) {
	return GetLastUserStreak(ctx, r.tx, streakID, userID)
}

func (rw readWrite) GetLastUserStreak(ctx context.Context, streakID, userID string) (*repo.UserStreak, error) {
	return GetLastUserStreak(ctx, rw.tx, streakID, userID)
}

func GetLastUserStreak(ctx context.Context, db Reader, streakID, userID string) (*repo.UserStreak, error) {
	index := tables.Get().UserStreaks().Indexes().ByStreakIDUserIDStartedAtDesc()
	key := spanner.Key{streakID, userID}.AsPrefix()
	opts := spanner.ReadOptions{
		Limit:      1,
		Index:      index.Name(),
		RequestTag: "GetLastUserStreak",
	}
	iter := db.ReadWithOptions(ctx, index.Table(), key, index.AllColumnNames(), &opts)
	return GetResultPtr[repo.UserStreak](iter)
}
