package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertStreakType(ctx context.Context, st repo.StreakType, name string) error {
	return UpsertStreakType(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), st, name)
}

func (rw readWrite) UpsertStreakType(ctx context.Context, st repo.StreakType, name string) error {
	return UpsertStreakType(ctx, rw.tx, st, name)
}

func UpsertStreakType(_ context.Context, db BufferWriter, st repo.StreakType, name string) error {
	table := tables.Get().StreakTypes()
	columns := table.Columns()

	op := spanner.InsertOrUpdate(
		table.TableName(),
		Cols{columns.StreakType(), columns.Name()},
		Vals{st, name},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
