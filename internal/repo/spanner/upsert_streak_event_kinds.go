package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertStreakEventKinds(ctx context.Context, streakID string, events ...repo.EventKind) error {
	return UpsertStreakEventKinds(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), streakID, events...)
}

func (rw readWrite) UpsertStreakEventKinds(ctx context.Context, streakID string, events ...repo.EventKind) error {
	return UpsertStreakEventKinds(ctx, rw.tx, streakID, events...)
}

func UpsertStreakEventKinds(_ context.Context, db BufferWriter, streakID string, events ...repo.EventKind) error {
	if len(events) == 0 {
		return nil
	}

	table := tables.Get().StreakEventKinds()
	tableName := table.TableName()
	columns := Cols{table.Columns().StreakID(), table.Columns().EventKind()}

	ops := make([]*spanner.Mutation, len(events))
	for i, kind := range events {
		ops[i] = spanner.InsertOrUpdate(tableName, columns, Vals{streakID, kind})
	}

	return db.BufferWrite(ops)
}
