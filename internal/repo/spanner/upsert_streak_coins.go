package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertStreakCoins(ctx context.Context, streakID string, coinsCount, everyStreakCount int64) error {
	return UpsertStreakCoins(
		ctx,
		w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()),
		streakID,
		coinsCount,
		everyStreakCount,
	)
}

func (rw readWrite) UpsertStreakCoins(ctx context.Context, streakID string, coinsCount, everyStreakCount int64) error {
	return UpsertStreakCoins(ctx, rw.tx, streakID, coinsCount, everyStreakCount)
}

func UpsertStreakCoins(_ context.Context, db BufferWriter, streakID string, coinsCount, everyStreakCount int64) error {
	table := tables.Get().StreakCoins()
	columns := table.Columns()

	op := spanner.InsertOrUpdate(
		table.TableName(),
		Cols{columns.StreakID(), columns.CoinsCount(), columns.EveryStreakCount()},
		Vals{streakID, coinsCount, everyStreakCount},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
