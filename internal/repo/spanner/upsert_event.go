package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertEvent(ctx context.Context, in repo.UpsertEventInput) error {
	return UpsertEvent(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), in)
}

func (rw readWrite) UpsertEvent(ctx context.Context, in repo.UpsertEventInput) error {
	return UpsertEvent(ctx, rw.tx, in)
}

func UpsertEvent(_ context.Context, db BufferWriter, in repo.UpsertEventInput) error {
	table := tables.Get().Events()
	columns := table.Columns()

	op := spanner.InsertOrUpdateMap(
		table.TableName(),
		map[string]interface{}{
			columns.ID():        in.ID,
			columns.AppID():     in.AppID,
			columns.UserID():    in.UserID,
			columns.CircleID():  in.CircleID,
			columns.EventKind(): in.EventKind,
			columns.CreatedAt(): in.CreatedAt,
		},
	)

	return db.BufferWrite([]*spanner.Mutation{op})
}
