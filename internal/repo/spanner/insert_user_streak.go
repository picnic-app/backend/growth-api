package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertUserStreak(ctx context.Context, in repo.InsertUserStreakInput) error {
	return InsertUserStreak(ctx, w.bufferWriter(ctx), in)
}

func (rw readWrite) InsertUserStreak(ctx context.Context, in repo.InsertUserStreakInput) error {
	return InsertUserStreak(ctx, rw.tx, in)
}

func InsertUserStreak(_ context.Context, db BufferWriter, in repo.InsertUserStreakInput) error {
	table := tables.Get().UserStreaks()
	columns := table.Columns()

	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			columns.ID():                      in.ID,
			columns.UserID():                  in.UserID,
			columns.StreakID():                in.StreakID,
			columns.StartedAt():               in.StartedAt,
			columns.PeriodDuration():          in.PeriodDuration,
			columns.LastBlockEndsAt():         in.LastBlockEndsAt,
			columns.LastBlockNumberOfEvents(): in.LastBlockNumberOfEvents,
			columns.NumberOfEventsPerPeriod(): in.NumberOfEventsPerPeriod,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
