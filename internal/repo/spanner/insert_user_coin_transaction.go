package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertUserCoinTransaction(ctx context.Context, id, userIDs string, amount int64) error {
	return InsertUserCoinTransaction(ctx, w.bufferWriter(ctx), id, userIDs, amount)
}

func (rw readWrite) InsertUserCoinTransaction(ctx context.Context, id, userIDs string, amount int64) error {
	return InsertUserCoinTransaction(ctx, rw.tx, id, userIDs, amount)
}

func InsertUserCoinTransaction(_ context.Context, db BufferWriter, id, userID string, amount int64) error {
	table := tables.Get().UserCoinTransactions()
	columns := table.Columns()

	op := spanner.Insert(
		table.TableName(),
		Cols{columns.ID(), columns.UserID(), columns.Amount(), columns.CreatedAt()},
		Vals{id, userID, amount, spanner.CommitTimestamp},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
