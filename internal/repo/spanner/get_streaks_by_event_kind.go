package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetStreaksByEventKind(ctx context.Context, kind repo.EventKind) ([]repo.StreakID, error) {
	return GetStreaksByEventKind(ctx, r.tx, kind)
}

func (rw readWrite) GetStreaksByEventKind(ctx context.Context, kind repo.EventKind) ([]repo.StreakID, error) {
	return GetStreaksByEventKind(ctx, rw.tx, kind)
}

func GetStreaksByEventKind(ctx context.Context, db Reader, kind repo.EventKind) ([]repo.StreakID, error) {
	table := tables.Get().StreakEventKinds()
	opts := spanner.ReadOptions{
		Index:      table.Indexes().ByEventKind().Name(),
		RequestTag: "GetStreaksByEventKind",
	}
	key := spanner.Key{kind}
	iter := db.ReadWithOptions(ctx, table.TableName(), key, Cols{table.Columns().StreakID()}, &opts)
	return GetResults[string](iter)
}
