package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) ExistsAffiliateLinkSignup(
	ctx context.Context,
	ownerID,
	userID repo.UserID,
) (bool, error) {
	return ExistsAffiliateLinkSignup(ctx, r.tx, ownerID, userID)
}

func (rw readWrite) ExistsAffiliateLinkSignup(
	ctx context.Context,
	ownerID,
	userID repo.UserID,
) (bool, error) {
	return ExistsAffiliateLinkSignup(ctx, rw.tx, ownerID, userID)
}

func ExistsAffiliateLinkSignup(
	ctx context.Context,
	db Reader,
	ownerID,
	userID repo.UserID,
) (bool, error) {
	return ExistsKey(ctx, db, tables.Get().AffiliateLinkSignups(), spanner.Key{ownerID, userID})
}
