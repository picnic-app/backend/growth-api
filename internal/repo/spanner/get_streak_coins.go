package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetStreakCoins(ctx context.Context, streakID string) ([]repo.StreakCoins, error) {
	return GetStreakCoins(ctx, r.tx, streakID)
}

func (rw readWrite) GetStreakCoins(ctx context.Context, streakID string) ([]repo.StreakCoins, error) {
	return GetStreakCoins(ctx, rw.tx, streakID)
}

func GetStreakCoins(ctx context.Context, db Reader, streakID string) ([]repo.StreakCoins, error) {
	table := tables.Get().StreakCoins()
	return GetByKeys[repo.StreakCoins](ctx, db, table, spanner.Key{streakID}.AsPrefix())
}
