package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) ExistsAffiliateLinkClick(
	ctx context.Context,
	ownerID repo.UserID,
	fingerprint repo.DeviceFingerprint,
) (bool, error) {
	return ExistsAffiliateLinkClick(ctx, r.tx, ownerID, fingerprint)
}

func (rw readWrite) ExistsAffiliateLinkClick(
	ctx context.Context,
	ownerID repo.UserID,
	fingerprint repo.DeviceFingerprint,
) (bool, error) {
	return ExistsAffiliateLinkClick(ctx, rw.tx, ownerID, fingerprint)
}

func ExistsAffiliateLinkClick(
	ctx context.Context,
	db Reader,
	ownerID repo.UserID,
	fingerprint repo.DeviceFingerprint,
) (bool, error) {
	return ExistsKey(ctx, db, tables.Get().AffiliateLinkClicks(), spanner.Key{ownerID, fingerprint})
}
