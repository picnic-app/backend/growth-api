package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpdateStreak(ctx context.Context, in repo.UpdateStreakInput) error {
	return UpdateStreak(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), in)
}

func (rw readWrite) UpdateStreak(ctx context.Context, in repo.UpdateStreakInput) error {
	return UpdateStreak(ctx, rw.tx, in)
}

func UpdateStreak(_ context.Context, db BufferWriter, in repo.UpdateStreakInput) error {
	table := tables.Get().Streaks()
	columns := table.Columns()

	m := map[string]interface{}{
		columns.ID():        in.StreakID,
		columns.UpdatedBy(): in.UserID,
		columns.UpdatedAt(): spanner.CommitTimestamp,
	}

	if in.Name != nil {
		m[columns.Name()] = in.Name
	}
	if in.Description != nil {
		m[columns.Description()] = in.Description
	}
	if in.EventsPerPeriod != nil {
		m[columns.EventsPerPeriod()] = in.EventsPerPeriod
	}
	if in.PeriodDuration != nil {
		m[columns.PeriodDuration()] = *in.PeriodDuration
	}

	op := spanner.UpdateMap(table.TableName(), m)
	return db.BufferWrite([]*spanner.Mutation{op})
}
