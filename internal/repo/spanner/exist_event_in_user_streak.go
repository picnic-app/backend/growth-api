package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) ExistEventInUserStreak(ctx context.Context, eventID, userStreakID string) (bool, error) {
	return ExistEventInUserStreak(ctx, r.tx, eventID, userStreakID)
}

func (rw readWrite) ExistEventInUserStreak(ctx context.Context, eventID, userStreakID string) (bool, error) {
	return ExistEventInUserStreak(ctx, rw.tx, eventID, userStreakID)
}

func ExistEventInUserStreak(ctx context.Context, db Reader, eventID, userStreakID string) (bool, error) {
	index := tables.Get().UserStreakEvents().Indexes().ByEventIDUserStreakID()
	key := spanner.Key{eventID, userStreakID}
	_, err := db.ReadRowUsingIndex(ctx, index.Table(), index.Name(), key, index.AllColumnNames()[:1])
	if spanner.ErrCode(err) == codes.NotFound {
		return false, nil
	}
	return true, err
}
