package spanner

import (
	"context"
	"time"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) RemoveStreak(ctx context.Context, streakID, userID string) error {
	return RemoveStreak(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), streakID, userID)
}

func (rw readWrite) RemoveStreak(ctx context.Context, streakID, userID string) error {
	return RemoveStreak(ctx, rw.tx, streakID, userID)
}

func RemoveStreak(_ context.Context, db BufferWriter, streakID, userID string) error {
	table := tables.Get().Streaks()
	columns := table.Columns()

	op := spanner.Update(
		table.TableName(),
		Cols{columns.ID(), columns.DeletedBy(), columns.DeletedAt()},
		Vals{streakID, userID, time.Now()},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
