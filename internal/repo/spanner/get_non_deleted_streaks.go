package spanner

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetNonDeletedStreaks(ctx context.Context, ids ...repo.ID) ([]repo.Streak, error) {
	return GetNonDeletedStreaks(ctx, r.tx, ids...)
}

func (rw readWrite) GetNonDeletedStreaks(ctx context.Context, ids ...repo.ID) ([]repo.Streak, error) {
	return GetNonDeletedStreaks(ctx, rw.tx, ids...)
}

func GetNonDeletedStreaks(ctx context.Context, db Reader, ids ...repo.ID) ([]repo.Streak, error) {
	index := tables.Get().Streaks().Indexes().ByIDIfNotDeleted()
	return GetByKeysFromIndex[repo.Streak](ctx, db, index, ids...)
}
