package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/grpc/codes"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetAffiliateLinkStatistics(
	ctx context.Context,
	ownerID repo.UserID,
) (repo.AffiliateLinkStatistics, error) {
	return GetAffiliateLinkStatistics(ctx, r.tx, ownerID)
}

func (rw readWrite) GetAffiliateLinkStatistics(
	ctx context.Context,
	ownerID repo.UserID,
) (repo.AffiliateLinkStatistics, error) {
	return GetAffiliateLinkStatistics(ctx, rw.tx, ownerID)
}

func GetAffiliateLinkStatistics(
	ctx context.Context,
	db Reader,
	ownerID repo.UserID,
) (repo.AffiliateLinkStatistics, error) {
	table := tables.Get().AffiliateLinksStatistics()
	stats, err := GetByKey[repo.AffiliateLinkStatistics](ctx, db, table, spanner.Key{ownerID})
	if err != nil {
		if spanner.ErrCode(err) == codes.NotFound {
			return repo.AffiliateLinkStatistics{}, repo.ErrNotFound
		}
		return repo.AffiliateLinkStatistics{}, err
	}

	return stats, nil
}
