package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpdateUserStreak(ctx context.Context, streakID string, in repo.UpdateUserStreakInput) error {
	return UpdateUserStreak(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), streakID, in)
}

func (rw readWrite) UpdateUserStreak(ctx context.Context, streakID string, in repo.UpdateUserStreakInput) error {
	return UpdateUserStreak(ctx, rw.tx, streakID, in)
}

func UpdateUserStreak(_ context.Context, db BufferWriter, streakID string, in repo.UpdateUserStreakInput) error {
	if in == (repo.UpdateUserStreakInput{}) {
		return nil
	}

	table := tables.Get().UserStreaks()
	columns := table.Columns()

	m := map[string]interface{}{columns.ID(): streakID}
	if in.LastBlockEndsAt != nil {
		m[columns.LastBlockEndsAt()] = in.LastBlockEndsAt
	}
	if in.LastBlockNumberOfEvents != nil {
		m[columns.LastBlockNumberOfEvents()] = in.LastBlockNumberOfEvents
	}

	op := spanner.UpdateMap(table.TableName(), m)
	return db.BufferWrite([]*spanner.Mutation{op})
}
