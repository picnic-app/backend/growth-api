package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetEventKindsByStreakType(ctx context.Context, st repo.StreakType) ([]repo.EventKind, error) {
	return GetEventKindsByStreakType(ctx, r.tx, st)
}

func (rw readWrite) GetEventKindsByStreakType(ctx context.Context, st repo.StreakType) ([]repo.EventKind, error) {
	return GetEventKindsByStreakType(ctx, rw.tx, st)
}

func GetEventKindsByStreakType(ctx context.Context, db Reader, st repo.StreakType) ([]repo.EventKind, error) {
	table := tables.Get().EventKindStreakTypes()
	key := spanner.Key{st}.AsPrefix()
	iter := db.Read(ctx, table.TableName(), key, []string{table.Columns().EventKind()})
	return GetResults[string](iter)
}
