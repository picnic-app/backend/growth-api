package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetStreakEventKinds(ctx context.Context, streakID string) ([]repo.EventKind, error) {
	return GetStreakEventKinds(ctx, r.tx, streakID)
}

func (rw readWrite) GetStreakEventKinds(ctx context.Context, streakID string) ([]repo.EventKind, error) {
	return GetStreakEventKinds(ctx, rw.tx, streakID)
}

func GetStreakEventKinds(ctx context.Context, db Reader, streakID string) ([]repo.EventKind, error) {
	table := tables.Get().StreakEventKinds()
	key := spanner.Key{streakID}.AsPrefix()
	iter := db.Read(ctx, table.TableName(), key, Cols{table.Columns().EventKind()})
	return GetResults[string](iter)
}
