package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertUserStreakEvent(ctx context.Context, eventID, streakID, userStreakID string) error {
	return InsertUserStreakEvent(ctx, w.bufferWriter(ctx), eventID, streakID, userStreakID)
}

func (rw readWrite) InsertUserStreakEvent(ctx context.Context, eventID, streakID, userStreakID string) error {
	return InsertUserStreakEvent(ctx, rw.tx, eventID, streakID, userStreakID)
}

func InsertUserStreakEvent(_ context.Context, db BufferWriter, eventID, streakID, userStreakID string) error {
	table := tables.Get().UserStreakEvents()
	columns := table.Columns()

	op := spanner.Insert(
		table.TableName(),
		Cols{columns.EventID(), columns.StreakID(), columns.UserStreakID()},
		Vals{eventID, streakID, userStreakID},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
