package spanner

import (
	"context"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetStreaks(ctx context.Context, ids ...repo.ID) ([]repo.Streak, error) {
	return GetStreaks(ctx, r.tx, ids...)
}

func (rw readWrite) GetStreaks(ctx context.Context, ids ...repo.ID) ([]repo.Streak, error) {
	return GetStreaks(ctx, rw.tx, ids...)
}

func GetStreaks(ctx context.Context, db Reader, ids ...repo.ID) ([]repo.Streak, error) {
	table := tables.Get().Streaks()
	return GetByKeys[repo.Streak](ctx, db, table, ids...)
}
