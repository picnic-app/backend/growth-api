package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) DeleteStreakEventKind(ctx context.Context, streakID, eventKind string) error {
	return DeleteStreakEventKind(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), streakID, eventKind)
}

func (rw readWrite) DeleteStreakEventKind(ctx context.Context, streakID, eventKind string) error {
	return DeleteStreakEventKind(ctx, rw.tx, streakID, eventKind)
}

func DeleteStreakEventKind(_ context.Context, db BufferWriter, streakID, eventKind string) error {
	table := tables.Get().StreakEventKinds()
	return Delete(db, table, spanner.Key{streakID, eventKind})
}
