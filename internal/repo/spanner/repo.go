package spanner

import (
	"context"

	"cloud.google.com/go/spanner"
	"google.golang.org/api/option"
)

func NewRepoWithClient(cli *spanner.Client) Repo { return Repo{cli: cli} }

type Repo struct{ cli *spanner.Client }

func NewSpannerClient(ctx context.Context, dsn, adcCredPath string) (*spanner.Client, error) {
	var opts []option.ClientOption
	if adcCredPath != "" {
		opts = append(opts, option.WithCredentialsFile(adcCredPath))
	}

	client, err := spanner.NewClient(ctx, dsn, opts...)
	if err != nil {
		return nil, err
	}

	return client, nil
}
