package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (r readOnly) GetUserCoins(ctx context.Context, userID string) (int64, error) {
	return GetUserCoins(ctx, r.tx, userID)
}

func (rw readWrite) GetUserCoins(ctx context.Context, userID string) (int64, error) {
	return GetUserCoins(ctx, rw.tx, userID)
}

func GetUserCoins(ctx context.Context, db Reader, userID string) (total int64, err error) {
	table := tables.Get().UserCoinTransactions()
	index := table.Indexes().ByUserIDCreatedAt()
	key := spanner.Key{userID}.AsPrefix()
	iter := db.ReadUsingIndex(ctx, index.Table(), index.Name(), key, Cols{table.Columns().Amount()})
	err = iter.Do(func(r *spanner.Row) error {
		var amount int64
		err = r.Columns(&amount)
		if err == nil {
			total += amount
		}
		return err
	})
	return total, err
}
