package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertEventKind(ctx context.Context, kind repo.EventKind) error {
	return UpsertEventKind(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), kind)
}

func (rw readWrite) UpsertEventKind(ctx context.Context, kind repo.EventKind) error {
	return UpsertEventKind(ctx, rw.tx, kind)
}

func UpsertEventKind(_ context.Context, db BufferWriter, kind repo.EventKind) error {
	table := tables.Get().EventKinds()
	columns := table.Columns()

	op := spanner.InsertOrUpdate(
		table.TableName(),
		Cols{columns.Kind()},
		Vals{kind},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
