package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) DeleteStreakCoins(ctx context.Context, streakID string, everyStreakCount int64) error {
	return DeleteStreakCoins(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), streakID, everyStreakCount)
}

func (rw readWrite) DeleteStreakCoins(ctx context.Context, streakID string, everyStreakCount int64) error {
	return DeleteStreakCoins(ctx, rw.tx, streakID, everyStreakCount)
}

func DeleteStreakCoins(_ context.Context, db BufferWriter, streakID string, everyStreakCount int64) error {
	table := tables.Get().StreakCoins()
	return Delete(db, table, spanner.Key{streakID, everyStreakCount})
}
