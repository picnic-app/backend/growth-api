package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) UpsertEventKindStreakTypes(ctx context.Context, kind repo.EventKind, st ...repo.StreakType) error {
	return UpsertEventKindStreakTypes(ctx, w.bufferWriter(ctx, spanner.ApplyAtLeastOnce()), kind, st...)
}

func (rw readWrite) UpsertEventKindStreakTypes(ctx context.Context, kind repo.EventKind, st ...repo.StreakType) error {
	return UpsertEventKindStreakTypes(ctx, rw.tx, kind, st...)
}

func UpsertEventKindStreakTypes(_ context.Context, db BufferWriter, kind repo.EventKind, st ...repo.StreakType) error {
	if len(st) == 0 {
		return nil
	}

	table := tables.Get().EventKindStreakTypes()
	tableName := table.TableName()
	columns := Cols{table.Columns().StreakType(), table.Columns().EventKind()}

	ops := make([]*spanner.Mutation, len(st))
	for i, st := range st {
		ops[i] = spanner.InsertOrUpdate(tableName, columns, Vals{st, kind})
	}

	return db.BufferWrite(ops)
}
