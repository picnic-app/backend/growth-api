package spanner

import (
	"context"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
	"gitlab.com/picnic-app/backend/growth-api/internal/repo/tables"
)

func (w writeOnly) InsertAffiliateLinkClick(
	ctx context.Context,
	ownerID,
	fingerprint repo.DeviceFingerprint,
) error {
	return InsertAffiliateLinkClick(ctx, w.bufferWriter(ctx), ownerID, fingerprint)
}

func (rw readWrite) InsertAffiliateLinkClick(
	ctx context.Context,
	ownerID,
	fingerprint repo.DeviceFingerprint,
) error {
	return InsertAffiliateLinkClick(ctx, rw.tx, ownerID, fingerprint)
}

func InsertAffiliateLinkClick(
	_ context.Context,
	db BufferWriter,
	ownerID,
	fingerprint repo.DeviceFingerprint,
) error {
	table := tables.Get().AffiliateLinkClicks()
	columns := table.Columns()
	op := spanner.InsertMap(
		table.TableName(),
		map[string]interface{}{
			columns.OwnerID():           ownerID,
			columns.DeviceFingerprint(): fingerprint,
			columns.CreatedAt():         spanner.CommitTimestamp,
		},
	)
	return db.BufferWrite([]*spanner.Mutation{op})
}
