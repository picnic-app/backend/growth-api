package spanner

import (
	"context"
	"time"

	"cloud.google.com/go/spanner"

	"gitlab.com/picnic-app/backend/growth-api/internal/repo"
)

type (
	readOnly  struct{ tx *spanner.ReadOnlyTransaction }
	writeOnly struct{ cli *spanner.Client }
	readWrite struct{ tx *spanner.ReadWriteTransaction }
)

var (
	_ repo.ReadActions      = readOnly{}
	_ repo.WriteActions     = writeOnly{}
	_ repo.ReadWriteActions = readWrite{}
)

func (r Repo) SingleRead() repo.ReadActions { return readOnly{tx: r.cli.Single()} }

func (r Repo) SingleWrite() repo.WriteActions { return writeOnly(r) }

func (r Repo) ReadOnlyTx(ctx context.Context, f func(context.Context, repo.ReadActions) error) error {
	tx := r.cli.ReadOnlyTransaction()
	defer tx.Close()
	return f(ctx, readOnly{tx: tx})
}

func (r Repo) ReadWriteTx(
	ctx context.Context,
	f func(context.Context, repo.ReadWriteActions) error,
) (time.Time, error) {
	return r.cli.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return f(ctx, readWrite{tx: tx})
	})
}

//nolint:unused
func (w writeOnly) tx(ctx context.Context, f func(ctx context.Context, tx repo.WriteActions) error) error {
	_, err := w.cli.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return f(ctx, readWrite{tx: tx})
	})
	return err
}

func (w writeOnly) bufferWriter(ctx context.Context, opts ...spanner.ApplyOption) bufferWriter {
	return bufferWriter{ctx: ctx, cli: w.cli, opts: opts}
}

// partitionedUpdater returns a restricted implementation of the Updater. Make
// sure you read the documentation.
//
// The DML statement must be fully partitionable: it must be expressible as the
// union of many statements each of which accesses only a single row of the
// table. The statement should also be idempotent, because it may be applied
// more than once. PartitionedUpdate returns an estimated count of the number of
// rows affected. The actual number of affected rows may be greater than the
// estimate.
//
//nolint:unused
func (w writeOnly) partitionedUpdater() partitionedUpdater { return partitionedUpdater(w) }

type bufferWriter struct {
	ctx  context.Context
	cli  *spanner.Client
	opts []spanner.ApplyOption
}

func (u bufferWriter) BufferWrite(m []*spanner.Mutation) error {
	_, err := u.cli.Apply(u.ctx, m, u.opts...)
	return err
}

// partitionedUpdater implements Updater. Make sure you read the documentation.
//
// The DML statement must be fully partitionable: it must be expressible as the
// union of many statements each of which accesses only a single row of the
// table. The statement should also be idempotent, because it may be applied
// more than once. PartitionedUpdate returns an estimated count of the number of
// rows affected. The actual number of affected rows may be greater than the
// estimate.
//
//nolint:unused
type partitionedUpdater struct{ cli *spanner.Client }

//nolint:unused
func (u partitionedUpdater) Update(ctx context.Context, stmt spanner.Statement) (int64, error) {
	return u.cli.PartitionedUpdate(ctx, stmt)
}
