package repo

type AffiliateLinkStatistics struct {
	OwnerID            string
	Clicks             int64
	Signups            int64
	MobileAppDownloads int64
}
