package pub

import (
	"gitlab.com/picnic-app/backend/libs/golang/eventbus"
)

type EventBusPublisher interface{}

func NewEventBusPublisher(client eventbus.Client) EventBusPublisher {
	return &eventBusPub{
		client: client,
	}
}

type eventBusPub struct {
	client eventbus.Client
}
