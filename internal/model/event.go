package model

import "time"

type EventKind = string

type Event struct {
	ID        string
	EventKind EventKind
	AppID     *string
	UserID    *string
	CircleID  *string
	CreatedAt time.Time
}
