package model

import (
	"time"
)

type StreakType = int64

const (
	StreakTypeUser StreakType = iota + 1
)

type CreateStreakInput struct {
	Name            string
	Description     string
	StreakType      StreakType
	EventsPerPeriod int64
	PeriodDuration  time.Duration
	EventKinds      []EventKind
}

type UpdateStreakInput struct {
	Name            *string
	Description     *string
	EventsPerPeriod *int64
	PeriodDuration  *time.Duration
}

type Streak struct {
	ID              string
	Name            string
	CreatedBy       string
	Description     string
	StreakType      StreakType
	EventsPerPeriod int64
	PeriodDuration  time.Duration
	CreatedAt       time.Time
	DeletedAt       *time.Time
	DeletedBy       *string
	UpdatedAt       *time.Time
	UpdatedBy       *string
}
