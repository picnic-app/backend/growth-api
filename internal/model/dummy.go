package model

type Dummy struct {
	ID     int64  `spanner:"id"`
	Name   string `spanner:"name"`
	UserID int64  `spanner:"user_id"`
}

func (d Dummy) GetColumnNames() []string {
	return []string{"id", "name", "user_id"}
}
