package model

type StreakCoin struct {
	StreakID         string
	CoinsCount       int64
	EveryStreakCount int64
}
