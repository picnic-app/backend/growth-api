package model

type AffiliateLinkStatistics struct {
	Clicks             int64
	Signups            int64
	MobileAppDownloads int64
}
