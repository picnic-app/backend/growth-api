package model

import "time"

type UserStreak struct {
	ID                      string
	UserID                  string
	StreakID                string
	LastBlockEndsAt         time.Time
	StartedAt               time.Time
	CompletedBlocksCount    int64
	LastBlockNumberOfEvents int64
	NumberOfEventsPerPeriod int64
	PeriodDuration          time.Duration
	StreakLastsDuration     time.Duration
}
