CREATE TABLE EventKinds
(
    Kind STRING(100) NOT NULL,
) PRIMARY KEY (Kind);

CREATE TABLE StreakTypes
(
    StreakType INT64 NOT NULL,
    Name       STRING(20) NOT NULL,
) PRIMARY KEY (StreakType);

CREATE TABLE EventKindStreakTypes
(
    EventKind  STRING(100) NOT NULL,
    StreakType INT64 NOT NULL,
    CONSTRAINT EventKindStreakTypes_EventKind_FK FOREIGN KEY (EventKind) REFERENCES EventKinds (Kind),
    CONSTRAINT EventKindStreakTypes_StreakType_FK FOREIGN KEY (StreakType) REFERENCES StreakTypes (StreakType),
) PRIMARY KEY (StreakType, EventKind);

CREATE TABLE Events
(
    ID        STRING(36) NOT NULL,
    EventKind STRING(100) NOT NULL,
    CreatedAt TIMESTAMP NOT NULL,
    AppID     STRING(36),
    UserID    STRING(36),
    CircleID  STRING(36),
    CONSTRAINT Events_EventKind_FK FOREIGN KEY (EventKind) REFERENCES EventKinds (Kind),
) PRIMARY KEY (ID);

CREATE
NULL_FILTERED INDEX Events_EventKind_CreatedAt ON Events (EventKind, CreatedAt) STORING (UserID);

CREATE
NULL_FILTERED INDEX Events_EventKind_UserID_CreatedAt_DESC ON Events (EventKind, UserID, CreatedAt DESC);

CREATE TABLE Streaks
(
    ID              STRING(36) NOT NULL,
    Name            STRING(100) NOT NULL,
    StreakType      INT64     NOT NULL DEFAULT (1),
    Description     STRING(1000) NOT NULL DEFAULT (''),
    PeriodDuration  INT64     NOT NULL,
    EventsPerPeriod INT64     NOT NULL DEFAULT (1),
    CreatedAt       TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp = true),
    CreatedBy       STRING(36) NOT NULL,
    UpdatedAt       TIMESTAMP OPTIONS (allow_commit_timestamp = true),
    UpdatedBy       STRING(36),
    DeletedAt       TIMESTAMP,
    DeletedBy       STRING(36),
    IDIfNotDeleted  STRING(36) AS (IF (DeletedAt IS NULL, ID, NULL)) STORED,
    CONSTRAINT Streaks_PeriodDuration_Check CHECK (PeriodDuration > 0),
    CONSTRAINT Streaks_EventsPerPeriod_Check CHECK (EventsPerPeriod >= 1),
    CONSTRAINT Streaks_StreakType_FK FOREIGN KEY (StreakType) REFERENCES StreakTypes (StreakType),
) PRIMARY KEY (ID);

CREATE
UNIQUE
NULL_FILTERED INDEX Streaks_IDIfNotDeleted
    ON Streaks (IDIfNotDeleted) STORING
    (Name, StreakType, Description, PeriodDuration, EventsPerPeriod, CreatedAt, CreatedBy, UpdatedAt, UpdatedBy);

CREATE TABLE StreakEventKinds
(
    StreakID  STRING(36) NOT NULL,
    EventKind STRING(36) NOT NULL,
    CONSTRAINT StreakEventKinds_StreakID_FK FOREIGN KEY (StreakID) REFERENCES Streaks (ID),
    CONSTRAINT StreakEventKinds_EventKind_FK FOREIGN KEY (EventKind) REFERENCES EventKinds (Kind),
) PRIMARY KEY (StreakID, EventKind);

CREATE INDEX StreakEventKinds_EventKind ON StreakEventKinds (EventKind);

CREATE TABLE UserStreaks
(
    ID                      STRING(36) NOT NULL,
    UserID                  STRING(36) NOT NULL,
    StreakID                STRING(36) NOT NULL,
    PeriodDuration          INT64     NOT NULL,
    NumberOfEventsPerPeriod INT64     NOT NULL,
    StartedAt               TIMESTAMP NOT NULL,
    LastBlockEndsAt         TIMESTAMP NOT NULL,
    LastBlockNumberOfEvents INT64     NOT NULL DEFAULT (1),
    StreakLastsDuration     INT64     NOT NULL AS (TIMESTAMP_DIFF(LastBlockEndsAt, StartedAt, NANOSECOND)) STORED,
    CompletedBlocksCount    INT64     NOT NULL AS (
        CAST (
        StreakLastsDuration/PeriodDuration - IF(LastBlockNumberOfEvents<NumberOfEventsPerPeriod, 1, 0)
        AS INT64
        )
        ) STORED,
    CONSTRAINT UserStreaks_PeriodDuration_Check CHECK (PeriodDuration > 0),
    CONSTRAINT UserStreaks_StreakID_FK FOREIGN KEY (StreakID) REFERENCES Streaks (ID),
    CONSTRAINT UserStreaks_NumberOfEventsPerPeriod_Check CHECK (NumberOfEventsPerPeriod >= 1),
    CONSTRAINT UserStreaks_StartedAt_LastBlockEndsAt_Check CHECK (LastBlockEndsAt > StartedAt),
) PRIMARY KEY (ID);

CREATE INDEX UserStreaks_StreakID_UserID_StartedAt_DESC
    ON UserStreaks (StreakID, UserID, StartedAt DESC) STORING
(PeriodDuration, NumberOfEventsPerPeriod, LastBlockEndsAt, LastBlockNumberOfEvents, StreakLastsDuration, CompletedBlocksCount);

CREATE INDEX UserStreaks_StreakID_UserID_CompletedBlocksCount_DESC
    ON UserStreaks (StreakID, UserID, CompletedBlocksCount DESC) STORING
(PeriodDuration, NumberOfEventsPerPeriod, StartedAt, LastBlockEndsAt, LastBlockNumberOfEvents, StreakLastsDuration);

CREATE TABLE UserStreakEvents
(
    EventID      STRING(36) NOT NULL,
    StreakID     STRING(36) NOT NULL,
    UserStreakID STRING(36) NOT NULL,
    CONSTRAINT UserStreakEvents_EventID_FK FOREIGN KEY (EventID) REFERENCES Events (ID),
    CONSTRAINT UserStreakEvents_StreakID_FK FOREIGN KEY (StreakID) REFERENCES Streaks (ID),
    CONSTRAINT UserStreakEvents_UserStreakID_FK FOREIGN KEY (UserStreakID) REFERENCES UserStreaks (ID),
) PRIMARY KEY (StreakID, UserStreakID, EventID);

CREATE UNIQUE INDEX UserStreakEvents_EventID_UserStreakID ON UserStreakEvents (EventID, UserStreakID);

CREATE TABLE StreakCoins
(
    StreakID         STRING(36) NOT NULL,
    CoinsCount       INT64 NOT NULL DEFAULT (1),
    EveryStreakCount INT64 NOT NULL DEFAULT (1),
    CONSTRAINT StreakCoins_StreakID_FK FOREIGN KEY (StreakID) REFERENCES Streaks (ID),
    CONSTRAINT StreakCoins_CoinsCount_Check CHECK (CoinsCount >= 1),
    CONSTRAINT StreakCoins_EveryStreakCount_Check CHECK (EveryStreakCount >= 1),
) PRIMARY KEY (StreakID, EveryStreakCount DESC);

CREATE TABLE UserCoinTransactions
(
    ID        STRING(36) NOT NULL,
    UserID    STRING(36) NOT NULL,
    Amount    INT64     NOT NULL,
    CreatedAt TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp = true),
) PRIMARY KEY (ID);

CREATE INDEX UserCoinTransactions_UserID ON UserCoinTransactions (UserID, CreatedAt) STORING (Amount);

CREATE TABLE StreakCoinTransactions
(
    ID       STRING(36) NOT NULL,
    StreakID STRING(36) NOT NULL,
    CONSTRAINT StreakCoinTransactions_StreakID_FK FOREIGN KEY (StreakID) REFERENCES Streaks (ID),
) PRIMARY KEY (ID), INTERLEAVE IN PARENT UserCoinTransactions ON DELETE CASCADE;
