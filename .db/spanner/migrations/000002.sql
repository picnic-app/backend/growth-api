CREATE TABLE AffiliateLinkClicks
(
    OwnerID    STRING(36) NOT NULL,
    DeviceFingerprint STRING(255) NOT NULL,
    CreatedAt TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp=true)
) PRIMARY KEY (OwnerID, DeviceFingerprint);

CREATE TABLE AffiliateLinkSignups
(
    OwnerID STRING(36) NOT NULL,
    UserID STRING(36) NOT NULL,
    CreatedAt TIMESTAMP NOT NULL OPTIONS (allow_commit_timestamp=true)
) PRIMARY KEY (OwnerID, UserID);

CREATE TABLE AffiliateLinksStatistics
(
    OwnerID    STRING(36),
    Clicks     INT64,
    Signups    INT64
) PRIMARY KEY (OwnerID);